({
    doInit: function(cmp, evt, helper){                
        var acctid = window.location.href;
        //helper.getTierOne(cmp);
        var action = cmp.get("c.getTierOne");
        var inputsel = cmp.find("categoria1");
        var opts=[];
        action.setCallback(this, function(a) {
            for(var i=0;i< a.getReturnValue().length;i++){
                opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
            }
            inputsel.set("v.options", opts);

        });
        $A.enqueueAction(action); 
    },
    onSelectChange2 : function(cmp, event, helper) {
    	var selected = cmp.find("categoria1").get("v.value");
        var action = cmp.get("c.getTierTwo");
        var inputsel = cmp.find("categoria2");
        var opts=[];
        action.setParams({tier1: selected});
        action.setCallback(this, function(a) {
            for(var i=0;i< a.getReturnValue().length;i++){
                opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
            }
            inputsel.set("v.options", opts);
        });
        $A.enqueueAction(action); 

	}
})