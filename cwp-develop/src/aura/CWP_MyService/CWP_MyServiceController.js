({
	setButtonVisiblility : function(component, event, helper) {
        var tabSelected= event.getParam('tabSelected');
        alert(tabSelected);
        if(tabSelected=== 'myService'){
            // Ejecutamos código que lanzaríamos en el doInit
            var comp = component.find('CWP_MyService');
            $A.util.removeClass(comp, 'slds-hide');
            
        }else {
            var comp = component.find('CWP_MyService');
            $A.util.addClass(comp, 'slds-hide');
        }
        if(tabSelected=== 'myService'){
            var buttonVisibility = component.get('c.getButtonVisibility');
            buttonVisibility.setCallback(this, function(response){
                var state = response.getState();
                if(component.isValid() && state === "SUCCESS"){
                    var booleanMap = response.getReturnValue();
                    component.set("v.showService", booleanMap['showService']);
                    component.set("v.showDocumentation", booleanMap['showDocumentation']);
                    component.set("v.showBilling", booleanMap['showBilling']);
                    component.set("v.showMarket", booleanMap['showMarket']);
                }else{
                    alert("fallo!");
                }
            });
            $A.enqueueAction(buttonVisibility);
        }
    },
    
    setMainImage : function(component, event){
        var tabSelected= event.getParam('tabSelected');
        alert(tabSelected);
        if(tabSelected==='myService'){
            var imgUrl;
            var mainInfo = component.get('c.getMainImage');
           /* component.set("v.informationMenu", '{!$label.c.PCA_My_Service_menu}');
            component.set("v.title", '{!$label.c.PCA_MyServiceText}');*/
            mainInfo.setCallback(this, function(response){
                var state = response.getState();
                if(component.isValid() && state === "SUCCESS"){
                    var mainInfo = response.getReturnValue();
                    imgUrl= "url('/servlet/servlet.FileDownload?file=" + mainInfo['imgId']+ "')";
                    alert(imgUrl);
                    setTimeout(
                        function(){
                            if(mainInfo['imgId'] != undefined){
                                document.getElementById('mainImage').style.backgroundImage =  imgUrl;
                                document.getElementById('mainImage').style.padding="2.5rem 4.0625rem";
                                document.getElementById('mainImage').style.minHeight="21.875rem";
                                document.getElementById('mainImage').style.backgroundSize="contain"
                            }else{
                                document.getElementById('mainImage').className = "tlf-slide";
                                
                            }
                            if(mainInfo['informationMenu'] != undefined){
                                component.set("v.informationMenu", mainInfo['informationMenu']);
                            }
                            if(mainInfo['title'] != undefined){
                                component.set("v.title", mainInfo['title']);
                            }
                    }, 1);
                }else{
                    alert("fallo!");
                }
            });
            $A.enqueueAction(mainInfo);
        }
    },
    
    changeTab : function(component, event, helper) {
        var evt = $A.get("e.c:CWP_menu_Evt");
    	var tabToOpen = event.target.id;
        evt.setParams({"tabSelected": tabToOpen});
        evt.fire();
    },
    
/*
    openInventory : function(component, event, helper) {
     	var evt = $A.get("e.c:CWP_EventChangeComponent");
        evt.setParams({"toOpen": 'inventory'});
        evt.fire();
    },
    
    openDocumentation : function(component, event, helper) {
     	var evt = $A.get("e.c:CWP_EventChangeComponent");
        evt.setParams({"toOpen": 'documentation'});
        evt.fire();
    },
    
    openBilling : function(component, event, helper) {
     	var evt = $A.get("e.c:CWP_EventChangeComponent");
        evt.setParams({"toOpen": 'billing'});
        evt.fire();
    },

	openMarket : function(component, event, helper) {
     	var evt = $A.get("e.c:CWP_EventChangeComponent");
        evt.setParams({"toOpen": 'market'});
        evt.fire();
    }*/
})