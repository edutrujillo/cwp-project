({
    
    loadInfo : function(component, event, helper){                
        var action = component.get("c.loadInfo");                          
        //Callback
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();
                if(returnValue != null){                    
                    returnValue = JSON.parse(returnValue);                    
                    var rowsArray = new Array();                     
                    returnValue.forEach(function(arrayItem){                                               	
                        
                        var cellsArray = new Array();                        
                        cellsArray.push(arrayItem.Id);	
                        cellsArray.push(arrayItem.Name);	
                        cellsArray.push(arrayItem.Description);	
                        cellsArray.push(arrayItem.LastModifiedDate);                        
                        
                        rowsArray.push(cellsArray);
                        
                    });
                    
                    returnValue = rowsArray;
                    component.set("v.listReportsRecords", returnValue);
                    
                    var index0 = component.get("v.inicio");                                                            
                    var indexN = component.get("v.fin");                    
                    var avance = component.get("v.avance");                                        
                    this.getPaginatedList(component, event, helper, returnValue, index0, indexN, avance); //paginacion de la lista de listas                    
                }
            }
        });
        $A.enqueueAction(action);                
    },
    
    /******/
    getPaginatedList : function(component, event, helper, listToProcess, index0, indexN, avance) {
        
        var retList=[];
        indexN = Math.min(indexN, listToProcess.length);
        for(var i = index0; i<indexN; i++){
            retList.push(listToProcess[i]);
        }
        var infoList = String(index0+1) + " - " + String(indexN) + ' Out of ' + String(listToProcess.length);
        component.set("v.infoList", infoList); //string con los datos de qué elementos están viendo, (1 a 10 de 118 elementos)
        component.set("v.reportRecordsList", listToProcess);//lista completa
        component.set("v.inicio", index0);//índice del primer elemento que se muestra respecto a la tabla global
        component.set("v.fin", indexN); //indice del último elemento que se muestra respecto a la tabla global
        component.set("v.avance", avance); //nuevo avance seleccionado
        component.set("v.listSize", listToProcess.length); //tamaño de la lista global
        component.set("v.reportRecordsListToShow", retList); //lista para mostrar (la que se pinta en el iterador)
    },
    
    /*
    getOrderedList : function(component, event, helper, fieldToOrder, direction){
        var table = component.get("v.listReportsRecords");//document.getElementById('assetTable');
        
        var columnToSort =[];
        var mapToOrder = new Object();
        for(var i =0; i<table.length; i++){
            if(mapToOrder.hasOwnProperty(table[i][fieldToOrder])){
                mapToOrder[table[i][fieldToOrder]].push(i);
            }else{
                var innerList =[i];
                mapToOrder[table[i][fieldToOrder]] = innerList;
            }
        }
        columnToSort = Object.keys(mapToOrder);
        
        columnToSort.sort();
        var inicio;
        var fin;
        var orderedList = [];
        debugger;
        if(direction==="DESC"){
            for(var i =columnToSort.length-1; i>=0; i--){
                for(var j = 0; j< mapToOrder[columnToSort[i]].length; j++){
                    orderedList.push(table[mapToOrder[columnToSort[i]][j]]);
                }
            }
        }else if(direction==="ASC"){
            for(var i =0; i< columnToSort.length; i++){
                for(var j = 0; j< mapToOrder[columnToSort[i]].length; j++){
                    orderedList.push(table[mapToOrder[columnToSort[i]][j]]);
                }
            }
        }
        this.getPaginatedList(component, event, helper, orderedList, 0, component.get("v.avance"), component.get("v.avance"));
    },
    */
    
    
})