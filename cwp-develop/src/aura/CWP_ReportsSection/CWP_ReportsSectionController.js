({
	doInit : function(component, event, helper) {
        
        var tabSelected= event.getParam('tabSelected');        
        if (tabSelected === 'pca_report'){            
            // Ejecutamos código que lanzaríamos en el doInit
            helper.loadInfo(component);
            
            var comp = component.find('CWP_Reports_div');            
            $A.util.removeClass(comp, 'slds-hide');
            
        }else {            
            var comp = component.find('CWP_Reports_div');            
            $A.util.addClass(comp, 'slds-hide');            
        }        
    },
    
    getReportId : function(component, event, helper){                
        var popUpReportStatus = component.get("v.reportRecordsList"); 
        var inicio = component.get("v.inicio");        
        var indexTotal = parseInt(inicio) + parseInt(event.currentTarget.dataset.index);        
        var selectedFactura = popUpReportStatus[indexTotal];       
        
        component.set("v.reportId", selectedFactura[0]);		
    },
    
    
     /***	PAGINATION AND SORTING	***/
    getNewAvance : function(component, event, helper){
        var paginationId = event.target.id
        var newAvance = document.getElementById(paginationId).value; //id de la picklist donde se seleccionan los valores de list size
        var retList = component.get("v.reportRecordsList"); //lista completa, sin paginar
        var indexN = Math.min(newAvance, retList.length);
        helper.getPaginatedList(component, event, helper, retList, 0, indexN, newAvance); //metodo que ejecuta la paginacion
    },   
    
    
    listPaginateForward : function(component, event, helper){        
        var retList = component.get("v.reportRecordsList");
        var index0 = parseInt(component.get("v.inicio"));	
        var indexN = parseInt(component.get("v.fin"));	
        var avance = parseInt(component.get("v.avance"));
        if(index0+avance<retList.length){
            index0 = index0 + avance;
            indexN = index0 + avance;
            indexN = Math.min(indexN, retList.length);
        	helper.getPaginatedList(component, event, helper, retList, index0, indexN, avance);
        }
        
    },
    
    listPaginateBackward : function(component, event, helper){
        var retList = component.get("v.reportRecordsList");
        var index0 = parseInt(component.get("v.inicio"));	
        var indexN = parseInt(component.get("v.fin"));	
        var avance = parseInt(component.get("v.avance"));
        if(indexN-avance>0){
            indexN = index0;
            index0 = indexN - avance;
            index0 = Math.max(index0, 0);
        	helper.getPaginatedList(component, event, helper, retList, index0, indexN, avance);
        }        
    },
    
})