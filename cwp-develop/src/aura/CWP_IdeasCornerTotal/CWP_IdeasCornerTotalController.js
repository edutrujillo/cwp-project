({
    handleEventAbrirDetalle : function(component, event, helper) {
        debugger;
        component.set("v.isDetail", event.getParam("isDetail"));
        component.set("v.listPopularIdeasWrapper", event.getParam("listPopularIdeasWrapper"));
        component.set("v.listRecentIdeasWrapper", event.getParam("listRecentIdeasWrapper"));
        component.set("v.listMyIdeasWrapper", event.getParam("listMyIdeasWrapper"));
        component.set("v.idDetail", event.getParam("idDetail"));
        component.set("v.isIdea", event.getParam("isIdea"));
        component.set("v.detalleIdea", event.getParam("detalleIdea"));
        component.set("v.isFAQ", event.getParam("isFAQ"));
        component.set("v.solutionsPlatino", event.getParam("solutionsPlatino"));
        component.set("v.detalleFAQ", event.getParam("detalleFAQ"));
        component.set("v.mapIdeaComments", event.getParam("mapIdeaComments"));
        component.set("v.commentWrapperList", event.getParam("commentWrapperList"));
    }, 
    
    handleAbrirIdeasCornerPrincipal : function(component, event, helper) {
       
            component.set("v.isDetail", false);
            var comp = component.find('CWP_IdeasCornerTotal_div');
            $A.util.removeClass(component, 'slds-hide');
       
    }   
})