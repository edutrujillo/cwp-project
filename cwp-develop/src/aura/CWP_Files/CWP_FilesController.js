({
	doInit : function(component, event, helper) {
        
        var action = component.get("c.getUserId");
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
            	component.set("v.Id", response.getReturnValue());	
            }
        });
        $A.enqueueAction(action);

        /*var action = component.get("c.getArch"); 
        //alert(event.getParam("userId"));
        //action.setParams({"identif": event.getParam("userId")});   
        //event.getParam("userId");
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.chatterfiles", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);*/
	},
    getValues : function(cmp, event) {
		var ShowResultValue = event.getParam("Id");
        //alert('ID usert: '+ShowResultValue);
		// set the handler attributes based on event data
		cmp.set("v.get_result", ShowResultValue);
	},
    updateRecord: function(component, event) {
		var action = component.get("c.getArchid"); 
        action.setParams({"identif": component.get("v.get_result")});   
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.chatterfiles", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    downloadfile : function(component, event, helper) {
        var id = event.target.getAttribute("data-id");       
    	alert('Document ID:' +id);
    	
        /*var actiondownload = component.get("c.DownloadAttachment");
    	actiondownload.setParams({
        	"DownloadAttachmentID": id
    	});

      	actiondownload.setCallback(this, function(b){
        component.set("v.Baseurl", b.getReturnValue());
	*/
    },
    preview : function(component, event, helper) {
   		$A.get('lightning:openFiles').fire({
    	recordIds: [component.get("v.contentId")]
   	});
  	}
})