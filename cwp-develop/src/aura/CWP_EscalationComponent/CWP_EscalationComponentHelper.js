({
	getTeam : function(component) {
        
        //alert("GetTeam");
        //alert(component.get("c.getTeam"));
        /*for (var name in component) {
  			alert(name);
		}*/        
        var action = component.get("c.getTeam");    
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
            		console.log(actionResult.getReturnValue());
        			component.set("v.listHierarchyWrapper", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	}
})