({
    doInit : function(cmp, evt, helper) {
        
        var acctid = window.location.href.indexOf('one/one.app');
        var accionLightning = window.location.href.indexOf('lightning');
        var qickAction = window.location.href.indexOf('apex/BI_G4C_visitas_c');

        if(acctid === -1 && accionLightning === -1 && qickAction === -1 && window.location.href.indexOf('BI_G4C_Clients_Home_VF')=== -1) {          
            helper.setDataInit(cmp, helper);
        } else if(qickAction > 0) {
            helper.setDataInit(cmp, helper);
        } else {
            helper.hideBack(cmp);
        }     
    },   
    iniciaComponente : function(cmp, evt, helper) {
        try{  
            cmp.set('v.account',evt.getParam("account"));
            helper.getEventsByAccount(cmp, helper);
        }catch(err) {
            //alert("BI_G4C_Visitas_Lista:[do Init] "+err.message);
        }
    },
    newVisit: function(cmp, evt, helper){
        helper.showVisitPanel(cmp);
    },
    openVisit: function(cmp, evt, helper){
        var events = cmp.get('v.events');
        var event = events[evt.currentTarget.dataset.index];
        helper.showVisitPanel(cmp, event.Id);
    },
    toggleCaducadas: function(cmp, evt, helper){
        var sc = cmp.get('v.showCaducadas');
        sc = !sc;
        cmp.set('v.showCaducadas', sc);
        helper.filterEvents(cmp);
    },
    toggleFinalizadas: function(cmp, evt, helper){
        var sc = cmp.get('v.showFinalizadas');
        sc = !sc;
        cmp.set('v.showFinalizadas', sc);
        helper.filterEvents(cmp);
    },
    refreshView: function(cmp,evt,helper){
        helper.getEventsByAccount(cmp, helper);
        cmp.set("v.body", '');
        var confirm = cmp.find('BI_G4C_Visitas_Nueva_id');
        $A.util.addClass(confirm, 'slds-hide');
        
        confirm = cmp.find('toastConfirm');
        $A.util.removeClass(confirm, 'slds-hide');
        
    },
    deleteVisit: function(cmp, evt, helper){
        helper.removeVisit(cmp, evt, helper);
    },
    showConfirm: function(cmp, evt, helper){
        var events = cmp.get('v.events');
        var eventoAEliminar = events[evt.currentTarget.dataset.index];
        cmp.set('v.eventoAEliminar', eventoAEliminar);
        var confirm = cmp.find('confirm-delete');
        $A.util.removeClass(confirm, 'slds-hide');
        
    },
    hideConfirm: function(cmp, evt, helper){
        helper.hideConfirm(cmp);
    }, 
    hideModalnewVisitas: function(cmp, evt, helper){
        
        cmp.set("v.body", '');
        var confirm = cmp.find('BI_G4C_Visitas_Nueva_id');
        
        $A.util.addClass(confirm, 'slds-hide');
    },
    closeError: function(cmp, evt, helper){
        var element = cmp.find('generic_error');
        $A.util.addClass(element,'slds-hide');
    },
    back: function(cmp, evt, helper){
        history.back();
    },
})