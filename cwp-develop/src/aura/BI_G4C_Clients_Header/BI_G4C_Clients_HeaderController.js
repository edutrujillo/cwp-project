({
    editAccount : function(component, event, helper) {
        var account = component.get('v.account');
        if (account!==undefined){
            var editRecord = $A.get("e.force:editRecord");
            editRecord.setParams({recordId: account.Id});
            editRecord.fire();
            
        }else {
            //alert('El usuario no es propietario de ningún cliente geolocalizado');
            var errorVar = $A.get("e.c:BI_G4C_Generic_error");
            errorVar.setParams({"header": "Error"});
            errorVar.setParams({"body": "El usuario no es propietario de ningún cliente geolocalizado"});
            errorVar.fire();
        }
    },
    viewAccount : function(component, event, helper) {
        var account = component.get('v.account');
        if (account!==undefined){
        	var lightningAppExternalEvent = $A.get("e.c:BI_G4C_call_Visual");
        	lightningAppExternalEvent.setParams({'accountRecord':account.Id});
        	lightningAppExternalEvent.fire();
            
            var editRecord = $A.get("e.force:navigateToSObject");
            if(editRecord != null ) {
                editRecord.setParams({recordId: account.Id});
                editRecord.fire();  
            } else {
            	//window.location.assign("/" +account.Id);
            }
        }else {
            //alert('El usuario no es propietario de ningún cliente geolocalizado');
            var errorVar = $A.get("e.c:BI_G4C_Generic_error");
            errorVar.setParams({"header": "Error"});
            errorVar.setParams({"body": "El usuario no es propietario de ningún cliente geolocalizado"});
            errorVar.fire();
        }
    },
    handleBookmarkClick: function(cmp, evt, helper){
        var bookButtonActive = cmp.get('v.account.BI_G4C_Bookmarked__c');
        
        var action = cmp.get("c.bookmarkClient");
        action.setParams({a:cmp.get('v.account'), b:(bookButtonActive)?"false":"true"});
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                var resAccs = response.getReturnValue();
                
                cmp.set('v.account', resAccs);
                
                var compEvent = cmp.getEvent("headerEvent");
                compEvent.setParams({account: resAccs, type:"Bookmark"});
                compEvent.fire();
            }
            else
            {
                
                //alert('error');
                var errorVar = $A.get("e.c:BI_G4C_Generic_error");
                errorVar.setParams({"header": "Error"});
                errorVar.setParams({"body": "Error"});
                errorVar.fire();
                
            }
        });
        $A.enqueueAction(action);
        
    },
    backToMain: function(cmp, evt, helper){
        
        var compEvent = cmp.getEvent("headerEvent");
        compEvent.setParams({account: null,type:"Back"});
        compEvent.fire();
    },
    doClearFilter: function(cmp, evt, helper){
        
        var compEvent = cmp.getEvent("headerEvent");
        compEvent.setParams({account: null,type:"Unfilter",campania:''});
        compEvent.fire();
    },
    showVisitas: function(cmp, evt, helper){
        
        var x = function(a){
            
        }
        
        var account = cmp.get('v.account');
        if (account!==undefined){
            var visitasEV = $A.get("e.c:BI_G4C_Modal_Visitas");
            visitasEV.setParams({"account": account});
            
            visitasEV.fire();
            
            /*  $A.createComponent(
              
                "c:BI_G4C_Visitas_Lista",
                {account: account,
                 isCommunity: cmp.get('v.isCommunity')},
                function(newCmp){
                    var cp = $A.get("e.force:showPanel");
                    cp.setParams({
                        "title":"Agendar visita",
                        "isFullScreen": false,
                        "lazyLoad": true,
                        "flavor": "medium",
                        "autoFocus": true,
                        "isTransient": true,
                        "removeAnimations": false,
                        "closeOnActionButtonClick": false,
                        "component": newCmp,
                        "onCreate": x,
                        "attributes": {
                            "animation": "bottom",
                            "closeAnimation": "bottom"
                        }});
                    cp.fire();
                }
            )*/
        }else {
            //alert('El usuario no es propietario de ningún cliente geolocalizado');
            var errorVar = $A.get("e.c:BI_G4C_Generic_error");
            errorVar.setParams({"header": "Error"});
            errorVar.setParams({"body": "El usuario no es propietario de ningún cliente geolocalizado"});
            errorVar.fire();
        }
    },
    test: function(cmp, evt, helper){
        /*if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                function(position){
                    var lat = position.coords.latitude;
                    var lon = position.coords.longitude;

                    alert(lat+' '+lon);
                });
        }
        else{
            alert('NO SOPORTADO');
        }*/
    }
    
    
})