({
	loadInfo : function(component) {
		var action = component.get("c.getAplis");     
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
                alert(returnValue);
            	if(returnValue != null){
                    if(returnValue == "")
                    component.set("v.appsAv", false);
        			component.set("v.apps", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	}
})