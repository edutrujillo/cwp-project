({
    doInit : function(component, event, helper) {
        var tabSelected= event.getParam('tabSelected');
        if(tabSelected=== 'pca_monitoreo'){
            helper.loadInfo(component);
            var comp = component.find('CWP_Monitoring_div');
            $A.util.removeClass(component, 'slds-hide');
        }else {
            var comp = component.find('CWP_Monitoring_div');
            $A.util.addClass(component, 'slds-hide');
        }
    }
})