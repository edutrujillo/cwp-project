({
    doInitHelper : function(component, event, helper){
        var userId = component.get("v.userId");
        var weekRaw = ['{!$Label.c.PCA_Domingo}','{!$Label.c.PCA_Lunes}','{!$Label.c.PCA_Martes}','{!$Label.c.PCA_Miercoles}','{!$Label.c.PCA_Jueves}','{!$Label.c.PCA_Viernes}','{!$Label.c.PCA_Sabado}'];
        var week = [];
        var firstWeekDay = parseInt(1);
        component.set("v.firstWeekDay", firstWeekDay);
        for(var i = firstWeekDay; i<weekRaw.length; i++){
            week.push(weekRaw[i]);
        }
        for(var i= 0; i<firstWeekDay; i++){
            week.push(weekRaw[i]);
        }
        component.set("v.weekList", week);
        var thisDate = new Date();
        
        var thisMonthB1 = thisDate.getMonth()+1;
        var thisYear = thisDate.getFullYear();       
        
        this.callToGetDaysList(component, thisMonthB1,thisYear,firstWeekDay,userId);
        this.callToGetInfo(component, userId);
        
    },
    
    callToGetInfo : function(component, userId){
		//alert(userId);
        var calendarInfo = component.get('c.getInfo');
        
        calendarInfo.setParams({
            "userId" : String(userId)
        });
        calendarInfo.setCallback(this, function(response){
        	var state = response.getState();
        	if(component.isValid() && state === "SUCCESS"){
                var infoMap = response.getReturnValue();
                component.set("v.calendarHeader", infoMap['calendarHeader']);
            }else{
                alert("fallo!");
            }
		});
 		$A.enqueueAction(calendarInfo);
    },
    
    callToGetDaysList : function(component, month, year, firstWeekDay, userId){
        //alert("id de usuario en helper	" + userId);
        //alert("mes:	" + month + "	año:	" + year + "	firstWeekDay:	"+firstWeekDay );
        var monthMap = new Object();
        monthMap[1] = "Enero";
        monthMap[2] = "Febrero";
        monthMap[3] = "Marzo";
        monthMap[4] = "Abril";
        monthMap[5] = "Mayo";
        monthMap[6] = "Junio";
        monthMap[7] = "Julio";
        monthMap[8] = "Agosto";
        monthMap[9] = "Septiembre";
        monthMap[10] = "Octubre";
        monthMap[11] = "Noviembre";
        monthMap[12] = "Diciembre";
        var monthYear = monthMap[month] + "  " + year;
        component.set("v.monthYear", monthYear);
        component.set("v.thisYear", year);
        component.set("v.thisMonth", month);
        
        var dayWrapList = component.get('c.getDayList');
        
        dayWrapList.setParams({
            "month1" : String(month),
            "year1" : String(year),
            "firstWeekDay" : String(firstWeekDay),
            "userId" : String(userId)
        });
        
        dayWrapList.setCallback(this, function(response){
        	var state = response.getState();
        	if(component.isValid() && state === "SUCCESS"){
                var theList = response.getReturnValue();
                component.set("v.daysList", theList);
                //alert(theList);
            }else{
                alert("fallo!");
            }
		});
 		$A.enqueueAction(dayWrapList);
    },  

})