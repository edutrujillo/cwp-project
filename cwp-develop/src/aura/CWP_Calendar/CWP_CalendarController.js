({
	/*************************************************************/
    /*queda pendiente borrar la variable del evento parameter container al salir del calendario*/
    doInit : function(component, event, helper) {
        var tabSelected= event.getParam('tabSelected');
        if(tabSelected=== 'calendar'){
            // Ejecutamos código que lanzaríamos en el doInit
            var comp = component.find('CWP_Calendar');
            $A.util.removeClass(comp, 'slds-hide');
            
        }else {
            var comp = component.find('CWP_Calendar');
            $A.util.addClass(comp, 'slds-hide');
        }
        var userId = event.getParam("userId");
        component.set("v.userId", userId);
        if(tabSelected==="calendar"){
            helper.doInitHelper(component, event, helper);
        }
    },
    
    
    getNextMonth : function(component, event, helper){
        var userId = component.get("v.userId");
        var thisMonth = component.get("v.thisMonth");
        var thisYear = component.get("v.thisYear");
        var firstWeekDay = component.get("v.firstWeekDay");
        var monthToGet = thisMonth+1;
        if(monthToGet==13){
            monthToGet=1;
            thisYear++;
        }
        helper.callToGetDaysList(component, monthToGet, thisYear, firstWeekDay, userId);
    },
    
    getPriorMonth : function(component, event, helper){
        var userId = component.get("v.userId");
        var thisMonth = component.get("v.thisMonth");
        var thisYear = component.get("v.thisYear");
        var firstWeekDay = component.get("v.firstWeekDay");
        var monthToGet = thisMonth-1;
        if(monthToGet==0){
            monthToGet=12;
            thisYear--;
        }
        helper.callToGetDaysList(component, monthToGet, thisYear, firstWeekDay, userId);
    }
    
})