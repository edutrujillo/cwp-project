({

        // Load expenses from Salesforce
        doInit: function(component, event, helper) {
        
            alert('entro en doInit');
            
            // Create the action
            var action = component.get("c.getChatterFiles");
        
            // Add callback behavior for when response is received
            action.setCallback(this, function(response) {
                
                var state = response.getState();
                
                alert('response status: '+state);
                
                if (component.isValid() && state === "SUCCESS") {
                   
                    alert('valid and success');
                    alert('return: '+response.getReturnValue());
                    
                    component.set("v.chatterfiles", response.getReturnValue());               
                }
                else {
                    console.log("Failed with state: " + state);
                }
            });
        
            // Send action off to be executed
            $A.enqueueAction(action);
        },
        createRecord : function (component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
          "recordId": "00QB0000000ybNX",
          "slideDevName": "related"
        });
        navEvt.fire();
    }
})