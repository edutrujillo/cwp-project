({
	getPopularIdeas : function(component){
        var action = component.get("c.obtainPopularIdeas");
        action.setParams({
        	"searchResult" : true,
        	"searchText" : component.get("v.searchText")
        });
        
        //Callback
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
            	var returnValue = response.getReturnValue();
                if(returnValue !== null){
					component.set("v.listPopularIdeasWrapper", returnValue);
				}
            }
        });
        $A.enqueueAction(action);
    },
    
    getRecentIdeas : function(component){
    	var action = component.get("c.obtenerIdeasRecientes");
    	action.setParams({
        	"searchResult" : true,
        	"searchText" : component.get("v.searchText")
        });
    	//Callback
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
            	var returnValue = response.getReturnValue();
                if(returnValue !== null){
					component.set("v.listRecentIdeasWrapper", returnValue);
				}
            }
        });
        $A.enqueueAction(action);
    },
    
    getMyIdeas : function(component){
    	var action = component.get("c.obtainMyIdeas");
    	action.setParams({
        	"searchResult" : true,
        	"searchText" : component.get("v.searchText")
        });
    	//Callback
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
            	var returnValue = response.getReturnValue();
                if(returnValue !== null){
					component.set("v.listMyIdeasWrapper", returnValue);
				}
            }
        });
        $A.enqueueAction(action);
    },
    
    getFAQs : function(component){
    	var action = component.get("c.getRecoverySolutions");
    	action.setParams({
    		"showAll" : true,
    		"searchResult" : true,
    		"searchText" : component.get("v.searchText") 
    	});
    	//Callback
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
            	var returnValue = response.getReturnValue();
                if(returnValue !== null){
					component.set("v.solutionsPlatino", returnValue);
				}
            }
        });
        $A.enqueueAction(action);
    }
    
})