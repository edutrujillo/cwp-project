({
		
	mostrarMasPop : function(component, event, helper){
		var limiteIdeas = component.get("v.popularIdeasNumber");
		limiteIdeas += 5;
		component.set("v.popularIdeasNumber", limiteIdeas);
	},
	
	mostrarMasMy : function(component, event, helper){
		var limiteIdeas = component.get("v.myIdeasNumber");
		limiteIdeas += 5;
		component.set("v.myIdeasNumber", limiteIdeas);
	},
	
	mostrarMasRec : function(component, event, helper){
		var limiteIdeas = component.get("v.recentIdeasNumber");
		limiteIdeas += 5;
		component.set("v.recentIdeasNumber", limiteIdeas);
	},
	
	mostrarMasFaqs : function(component, event, helper){
		var limiteFaqs = component.get("v.faqsNumber");
		limiteFaqs += 5;
		component.set("v.faqsNumber", limiteFaqs);
	},
	
	getPopIdeaId : function(component, event, helper){
    	var popIdeas = component.get("v.listPopularIdeasWrapper");
    	var selectedIdea = popIdeas[event.currentTarget.dataset.index].id;
    	component.set("v.selectedIdea", selectedIdea);
    	component.set("v.detalleIdea", popIdeas[event.currentTarget.dataset.index]);
    	var commentMap = component.get("v.mapIdeaComments");
    	component.set("v.commentWrapperList", commentMap[selectedIdea]);
    },
    
    getRecIdeaId : function(component, event, helper){
    	var recIdeas = component.get("v.listRecentIdeasWrapper");
    	var selectedIdea = recIdeas[event.currentTarget.dataset.index].id;
    	component.set("v.selectedIdea", selectedIdea);
    	component.set("v.detalleIdea", recIdeas[event.currentTarget.dataset.index]);
    	var commentMap = component.get("v.mapIdeaComments");
    	component.set("v.commentWrapperList", commentMap[selectedIdea]);
    },
    
    getMyIdeaId : function(component, event, helper){
    	var myIdeas = component.get("v.listMyIdeasWrapper");
    	var selectedIdea = myIdeas[event.currentTarget.dataset.index].id;
    	component.set("v.selectedIdea", selectedIdea);
    	component.set("v.detalleIdea", myIdeas[event.currentTarget.dataset.index]);
    	var commentMap = component.get("v.mapIdeaComments");
    	component.set("v.commentWrapperList", commentMap[selectedIdea]);
    },
    
    getFAQId : function(component, event, helper){
    	var faqs = component.get("v.solutionsPlatino");
    	//var selectedIdea = faqs[event.currentTarget.dataset.index].id;
    	//component.set("v.selectedIdea", selectedIdea);
    	component.set("v.detalleFAQ", faqs[event.currentTarget.dataset.index]);
    },
    
    searchRecords : function(component, event, helper){
    	if(component.get("v.isIdea")){
    		helper.getPopularIdeas(component);
    		helper.getRecentIdeas(component);
    		helper.getMyIdeas(component);
    	}else if(component.get("v.isFAQ")){
    		helper.getFAQs(component);
    	}
    }, 
    
    voteMas : function(component, event, helper){
    	event.preventDefault();
    	var action = component.get("c.voteIdea");
    	action.setParams({
    		"voteOkOrKo" : 'OK',
    		"selectedIdea" : component.get("v.detalleIdea").id
    	});
    	//Callback
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
            	var returnValue = response.getReturnValue();
                if(returnValue === true){
					alert($A.get("$Label.c.CWP_AlreadyVoted"));
				}else{
					helper.getPopularIdeas(component);
					helper.getRecentIdeas(component);
					helper.getMyIdeas(component);
					var updatedVote = parseInt(component.get("v.detalleIdea.VoteTotal"), 10) + parseInt(10,10);
					component.set("v.detalleIdea.VoteTotal", updatedVote);
				}
            }
        });
        $A.enqueueAction(action); 
    },
    
    voteMenos : function(component, event, helper){
    	var action = component.get("c.voteIdea");
    	action.setParams({
    		"voteOkOrKo" : 'KO',
    		"selectedIdea" : component.get("v.detalleIdea").id
    	});
    	//Callback
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
            	var returnValue = response.getReturnValue();
                if(returnValue === true){
					alert($A.get("$Label.c.CWP_AlreadyVoted"));
				}else{
					helper.getPopularIdeas(component);
					helper.getRecentIdeas(component);
					helper.getMyIdeas(component);
					var updatedVote = component.get("v.detalleIdea.VoteTotal") - 10;
					component.set("v.detalleIdea.VoteTotal", updatedVote);
				}
            }
        });
        $A.enqueueAction(action);
    },
    
    createNewComment : function(component, event, helper){
    	var newComment = component.find("newComment").get("v.value");
    	var action = component.get("c.createComment");
    	action.setParams({
    		"commentBody" : newComment,
    		"ideaId" : component.get("v.detalleIdea").id
    	});
    	//Callback
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
            	var returnValue = response.getReturnValue();
                if(returnValue !== null){
					var commentList = component.get("v.commentWrapperList");
					commentList = commentList.concat(returnValue);
					component.set("v.commentWrapperList", commentList);
					 $('#tlf-comment').modal('toggle');
					component.find("newComment").set("v.value", " ");
				}
            }
        });
    	$A.enqueueAction(action);
    },
    
    callCreateNewIdea : function(component, event, helper){
    	var title = component.find("newIdeaTitle").get("v.value");
    	var body = component.find("newIdea").get("v.value");
    	var action = component.get("c.insertIdea");
    	action.setParams({
    		"title" : title,
    		"body" : body
    	});
    	//Callback
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
            	var returnValue = response.getReturnValue();
                if(returnValue !== null){
					$('#tlf-add').modal('toggle');
					component.find("newIdeaTitle").set("v.value", " ");
					component.find("newIdea").set("v.value", " ");
					helper.getPopularIdeas(component);
					helper.getRecentIdeas(component);
					helper.getMyIdeas(component);
				}
            }
        });
    	$A.enqueueAction(action);
    }
})