({
	getOptions : function(cmp) {
		var action = cmp.get("c.getOptions");  		
   		action.setCallback(this, function(response) {
	   		if (cmp.isValid() && response.getState() === "SUCCESS"){	   			
	   			var opts = JSON.parse(response.getReturnValue());					
				cmp.find("InputSelectDynamic").set("v.options", opts);
	   		}
   		});
   		$A.enqueueAction(action);
	},
	clearFilterData: function(cmp){
		//console.log(cmp.find('filter-div'));
		//by JB 29/07
		cmp.set('v.filterValue', '');
		cmp.find('operator-selector').set('v.value', 'Equals');
		$A.util.addClass(cmp.find('filter-div'), 'slds-hide');
	}
})