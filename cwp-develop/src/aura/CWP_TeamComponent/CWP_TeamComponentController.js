({
    doInit : function(component, event, helper) {
        debugger;
        
        helper.getTeam(component);
        helper.getEscalation(component);
        
    },
    
    /*FOT 02032017 - method to get the id of the user of whom a meeting is getting requested and open the calendar with its events*/
    getUserId : function(component, event, helper){
        var userId = event.target.id;
        var evt = $A.get("e.c:CWP_EventChangeComponent");
        evt.setParams({ "toOpen": 'calendar', "userId": userId});
        evt.fire();
    },
    
    getCalendar : function(component, event, helper){
        var evt = $A.get("e.c:CWP_menu_Evt");
        var idx = event.target.id;
        evt.setParams({"tabSelected": 'calendar', "userId":idx});
        evt.fire();
        /*
        var container = component.find("container");
        $A.createComponent("c:CWP_Calendar",
                           {userId: '00525000001gjyZAAQ'},
                           function(cmp) {
                               container.set("v.body", [cmp]);
                           });*/
    },    
    
    readyPrint : function(component, event, helper){
        var readyMap = component.get("v.mapToReady");
        
        readyMap[event.getParam("keyToMap")] = true;
        component.set("v.mapToReady", readyMap);
        var isReady = true;
        
        var key;
        for(key in readyMap){
            if(readyMap[key] == false){
                isReady = false;
            }
        }
        
        if(isReady){
            
            setTimeout(function(){
                debugger;
                component.set("v.loaded", true);
                if($("[data-function='data-selectpicker']").length>0){
                    $("[data-function='data-selectpicker']").selectpicker();
                }
                if($("[data-function='data-carousel__team_uno']").length>0){
                    $("[data-function='data-carousel__team_uno']").slick({
                        infinite: true,
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        autoplay: false,
                        arrows:true,
                        dots:false,
                        draggable:false,
                        centerMode:true,
                        speed: 500,
                        
                        responsive:[
                            {
                                breakpoint:1024,
                                settings:{
                                    slidesToShow:2
                                }
                            },
                            {
                                breakpoint:768,
                                settings:{
                                    slidesToShow:1
                                }
                            }
                        ]
                    });
                }
                if($("[data-function='data-carousel__team']").length>0){
                    $("[data-function='data-carousel__team']").slick({
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        autoplay: false,
                        arrows:true,
                        dots:false,
                        draggable:false,
                        centerMode:true,
                        speed: 500,
                        infinite: true,
                        variableWidth: false,
                        
                        responsive:[
                            {
                                breakpoint:1024,
                                settings:{
                                    slidesToShow:2
                                }
                            },
                            {
                                breakpoint:768,
                                settings:{
                                    slidesToShow:1
                                }
                            }
                        ]
                    });
                }
            }, 1);
            for(key in readyMap){
                readyMap[key] = false;
            }
            component.set("v.mapToReady",readyMap);
            component.set("v.load","true");
        }
    },openDetailContact : function(component, event, helper){
        debugger;
        var contactos = component.get("v.listHierarchyWrapper");
        var contact = contactos[event.currentTarget.dataset.index];        
        var modal = $A.get("e.c:CWP_EquipoTelefonicaModalEvent");
        modal.setParams({
            "id": contact.UsernameId,
            "clase": 'Equipo'
            
        });    
        modal.fire();
        
    },
    
    
    
})