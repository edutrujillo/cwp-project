({
	doInit : function(component, event, helper) {
        
        var action = component.get("c.getUsers");
        component.set("v.paginaActual", 1);
        action.setParams({ "indice" : component.get("v.paginaActual") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.users", response.getReturnValue());
                //this.updateTotal(component);
            }
        });
        $A.enqueueAction(action);
        
        var actionUsersTotal = component.get("c.getTotalUsers");
        actionUsersTotal.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.numUsers", response.getReturnValue());
                var pag = response.getReturnValue();
                var paginas = parseInt(pag / 50);
                var resto = pag%50;
                if(resto > 0){
                    paginas++;
                }
                var numPags = component.get("v.selectOptions");
                for(var i = 1 ; i < paginas+1 ; i++){
                	numPags.push(i);  
                    document.getElementById("listaPaginas").innerHTML += '<option value="'+i+'">'+i+'</option>';
                }
                component.set("v.selectOptions", numPags);
                component.set("v.index",1); 
            }
        });
        $A.enqueueAction(actionUsersTotal);
    },
    
    onClick: function(component, event, helper) { 
        
        var index = component.get("v.index");
        var totalUsers = parseInt(component.get("v.numUsers"));
        if(index + 50 < totalUsers){
            index = parseInt(index) + 50;
            component.set("v.index",index); 
            var pag = component.get("v.paginaActual");
        	pag = parseInt(pag) + 1;
        	component.set("v.paginaActual", pag);
        }
        
		var action = component.get("c.getUsers"); 
        action.setParams({ "indice" : component.get("v.paginaActual") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.users", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    
    onClickBack: function(component, event, helper) { 
        
        var index = component.get("v.index");
        index = parseInt(index) - 50;
        if(index < 1){
            index = 1;
        }
        component.set("v.index",index); 
        
        var pag = component.get("v.paginaActual");
        pag = parseInt(pag) -1;
        component.set("v.paginaActual", pag);
        
		var action = component.get("c.getUsers"); 
        action.setParams({ "indice" : component.get("v.paginaActual") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.users", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},//Delimiter for future code
    
	onChange: function(component, event, helper) { 
        
        var idx = parseInt(event.target.value);
        if(idx == 1){
            component.set("v.index", 1);
        }else{
            component.set("v.index",(idx-1)*50);
        }
		component.set("v.paginaActual",idx);
        alert(idx);
        var action = component.get("c.getUsers"); 
        action.setParams({ "indice" : component.get("v.paginaActual") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (component.isValid() && state === "SUCCESS") {
                alert("ok");
                component.set("v.users", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},    
    
})