({
    ini: function(component) {  
        debugger;
        var action = component.get("c.getAttr"); 
        var dynamicCmp = component.find("InputSelectDynamic");
        action.setParams({ "indice" : component.get("v.index"), "numRec" : component.get("v.numRecords"), "page" : dynamicCmp.get("v.value") });
        
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){ 
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			//component.set("v.fieldSet", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    onClick: function(component) {  
        debugger;
        var action = component.get("c.getNext");   
        var dynamicCmp = component.find("InputSelectDynamic");
        action.setParams({ "indice" : component.get("v.index"), "numRec" : component.get("v.numRecords"), "page" : dynamicCmp.get("v.value")  });
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.fieldSet", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    indice : function(component) {
        
        var action = component.get("c.getIndice");    
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.index", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    numRecords : function(component) {
        
        var action = component.get("c.getNumRecords");    
        var dynamicCmp = component.find("InputSelectDynamic");
        component.set("v.numRecords",dynamicCmp.get("v.value"));
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.total", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
})