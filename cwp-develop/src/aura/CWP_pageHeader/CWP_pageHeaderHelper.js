({
    getInicialValues : function(cmp, event, helper) {
        var action = cmp.get("c.CWP_doInit");
        action.setCallback(this, function(response) {
            
            var errorVar;
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                var res = response.getReturnValue();
         			
                helper.setDynamicMenu(cmp, JSON.parse(res["dynamicHeader"]));
                 cmp.set('v.runningUser', JSON.parse(res["usuarioPortal"]));
                var accountsList=JSON.parse(res["listUsers"]);
                debugger;
                //cmp.set('v.listUsers', accountsList);
                
                //cmp.set('v.listUsers', accountsList);
                }
            else{/*LANZA ERROR
                errorVar = $A.get("e.c:BI_G4C_Generic_error");
                errorVar.setParams({"header": "Error"});
                errorVar.setParams({"body": "Error"});
                errorVar.fire();
                */
                
            }
        });
        $A.enqueueAction(action);
    }, 
    
    setDynamicMenu:function(cmp, list){
        
        var toggleText;
        for (var idDes in list){
            if(!list[idDes]){
                toggleText= cmp.find(idDes);
                $A.util.toggleClass(toggleText, "toggle");
                toggleText= cmp.find(idDes+"--m");
                $A.util.toggleClass(toggleText, "toggle");
            }		
        }
        if(!list["pca_equipotelefonica"] && !list["pca_customcalendar"] && !list["pca_chattercustom"] ){
            toggleText= cmp.find("pca_rManagementL");
            $A.util.toggleClass(toggleText, "toggle");
            toggleText= cmp.find("pca_rManagementL"+"--m");
            $A.util.toggleClass(toggleText, "toggle");
        }
        if(!list["pca_report"] && !list["pca_monitoreo"] && !list["pca_reclamospedidos"] && !list["pca_portales"]){
            toggleText= cmp.find("pca_sTrackingL");
            $A.util.toggleClass(toggleText, "toggle");
            toggleText= cmp.find("pca_rManagementL"+"--m");
            $A.util.toggleClass(toggleText, "toggle");
        }
        if(!list["pca_inventory"] && !list["pca_documentmanagement"] && !list["pca_facturacioncobranza"] && !list["pca_catalogo"]){
            toggleText= cmp.find("pca_mServiceL");
            $A.util.toggleClass(toggleText, "toggle");
            toggleText= cmp.find("pca_rManagementL"+"--m");
            $A.util.toggleClass(toggleText, "toggle");
        }
    }    
    
})