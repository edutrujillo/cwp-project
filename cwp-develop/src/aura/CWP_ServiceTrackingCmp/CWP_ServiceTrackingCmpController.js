({
    doInit : function(component, event) {
        debugger;
        var tabSelected= event.getParam('tabSelected');
        if(tabSelected=== 'pca_sTrackingL'){
            // Ejecutamos código que lanzaríamos en el doInit
            var comp = component.find('CWP_ServiceTracking_div');
            $A.util.removeClass(comp, 'slds-hide');
            
        }else {
            var comp = component.find('CWP_ServiceTracking_div');
            $A.util.addClass(comp, 'slds-hide');
        }
        
        
    },    
    
    setButtonVisiblility : function(component, event) {
        var displaying = event.getParam("tabSelected");
        if(displaying==='pca_sTrackingL'){
            var buttonVisibility = component.get('c.getButtonVisibility');
            buttonVisibility.setCallback(this, function(response){
                var state = response.getState();
                if(component.isValid() && state === "SUCCESS"){
                    var booleanMap = response.getReturnValue();
                    component.set("v.showReports", booleanMap['showReports']);
                    component.set("v.showMonitoreo", booleanMap['showMonitoreo']);
                    component.set("v.showPedido", booleanMap['showPedido']);
                    component.set("v.showPortales", booleanMap['showPortales']);
                }else{
                    alert("fallo en service tracking - visibilidad de botones");
                }
                /*if(state==="ERROR"){
                    var errors = response.getError();
                    if(errors){
                        if(errors[0] && errors[0].message) {
                            alert("Error message: " + errors[0].message);
                        }
                    }else{
                        console.log("Unknown error");
                    }
                }*/
            });
            $A.enqueueAction(buttonVisibility);
        }   
        
    },
    
    setMainImage : function(component, event){
        var displaying = event.getParam("tabSelected");
        if(displaying==='pca_sTrackingL'){
            var imgUrl;
            var mainInfo = component.get('c.getMainImage');
            component.set("v.informationMenu", '{!$label.c.PCA_My_Service_menu}');
            component.set("v.title", '{!$label.c.PCA_MyServiceText}');
            mainInfo.setCallback(this, function(response){
                var state = response.getState();
                if(component.isValid() && state === "SUCCESS"){
                    var mainInfo = response.getReturnValue();
                    imgUrl= "url('/servlet/servlet.FileDownload?file=" + mainInfo['imgId']+ "')";
                    setTimeout(
                        function(){
                            if(mainInfo['imgId'] != undefined){
                                document.getElementById('mainImage').style.backgroundImage =  imgUrl;
                                document.getElementById('mainImage').style.padding="2.5rem 4.0625rem";
                                document.getElementById('mainImage').style.minHeight="21.875rem";
                                document.getElementById('mainImage').style.backgroundSize="contain"
                            }else{
                                document.getElementById('mainImage').className = "tlf-slide";
                                
                            }
                            if(mainInfo['informationMenu'] != undefined){
                                component.set("v.informationMenu", mainInfo['informationMenu']);
                            }
                            if(mainInfo['title'] != undefined){
                                component.set("v.title", mainInfo['title']);
                            }
                        }, 1);
                }else{
                    alert("fallo en service tracking: background image");
                }
                /*if(state==="ERROR"){
                    var errors = response.getError();
                    if(errors){
                        if(errors[0] && errors[0].message) {
                            alert("Error message: " + errors[0].message);
                        }
                    }else{
                        console.log("Unknown error");
                    }
                }*/
            });
            $A.enqueueAction(mainInfo);
        }
    },
    
    changeTab : function(component, event, helper) {
        var evt = $A.get("e.c:CWP_menu_Evt");
    	var tabToOpen = event.target.id;
        evt.setParams({"tabSelected": tabToOpen});
        evt.fire();
    }
})