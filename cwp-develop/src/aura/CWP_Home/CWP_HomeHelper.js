({
    spinner : function(component, show){
        /* Variable Definition */
        var component = component.find('spinner');
        
        if(show) $A.util.removeClass(component,'slds-hide');
        else $A.util.addClass(component,'slds-hide');
        
    },
    getUser : function(component, event, helper){
        /* Attibute Initialization */
        component.set('v.user', null);
        
        /* Execute Apex Method */
        var action = component.get("c.getUserInfo");
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();
                if(returnValue != null){
                    component.set('v.user', returnValue);
                }
            }
        });
        $A.enqueueAction(action);
    },
    getAccounts : function(component, event, helper){
        /* Attibute Initialization */
        component.set('v.accounts', []);
        
        /* Execute Apex Method */
        var action = component.get("c.getUserAccounts");
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();
                if(returnValue != null){
                    component.set('v.accounts', returnValue);
                }
            }
        });
        $A.enqueueAction(action);
    }
})