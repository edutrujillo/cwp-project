({
    doInit : function (component, event, helper) { 
        /*helper.getUser(component, event, helper);
        helper.getAccounts(component, event, helper);*/
        debugger;
         var compEvent =  $A.get("e.c:CWP_menu_Evt")
        compEvent.setParams({"tabSelected" : "pca_home" });
        compEvent.fire();         
         
    },
    
    getCarouselImage : function(component, event,helper){
        debugger;
        var tabSelected= event.getParam('tabSelected');
        if(tabSelected=== 'pca_home'){
            var imageList = component.get('c.getImageList');
            
            imageList.setCallback(this, function(response){
                var state = response.getState();
                if(component.isValid() && state === "SUCCESS"){
                    var imageWrapList = response.getReturnValue();
                    setTimeout( function(){
                        component.set("v.imageList", imageWrapList);
                        var paintedList = document.getElementsByClassName("tlf-slider__item item active");
                        for(var i = 0; i<paintedList.length; i++){
                            paintedList[i].className = "tlf-slider__item item";
                        }
                        component.set("v.showImages", imageWrapList.length>0);
                    }, 1);
                    
                }else{
                    alert("fallo!");
                }
            });
            $A.enqueueAction(imageList);
            
            var comp = component.find('CWP_Home_div');
            $A.util.removeClass(comp, 'slds-hide');
            
        }else {
            var comp = component.find('CWP_Home_div');
            $A.util.addClass(comp, 'slds-hide');
        }
        
        
        
    },
    
    displayComponent : function (component, event, helper) {
        /* onCLick Element Data-Id */
        var divs = component.find("CWP_Container").getElement().childNodes;
        var auraId = event.target.getAttribute('data-id');
        
        /* Hide all */
        for(var item in divs){
            $A.util.addClass(divs[item], "slds-hide");
        }
        
        /* Display Selected Item */
        var element = component.find(auraId+'_Component');
        $A.util.removeClass(element, "slds-hide");
        
        /* Setup Selector */
        var desktop = component.find('Desktop_'+auraId+'_Selector');
        var device = component.find('Device_'+auraId+'_Selector');
        $A.util.addClass(desktop, "selected");
        $A.util.addClass(device, "active");
    },
    
    openRelationshipManagement : function(component, event, helper) {
   /*   var evt = $A.get("e.c:CWP_EventChangeComponent");
        evt.setParams({"toOpen": 'relationship'});
        evt.fire();*/
        
        var compEvent =  $A.get("e.c:CWP_menu_Evt")
        compEvent.setParams({"tabSelected" : "relationship" });
        compEvent.fire();
        
        
    },
    
    openServiceTracking : function(component, event, helper) {
        /*var evt = $A.get("e.c:CWP_EventChangeComponent");
        evt.setParams({"toOpen": 'tracking'});
        evt.fire();*/
        var compEvent =  $A.get("e.c:CWP_menu_Evt")
        compEvent.setParams({"tabSelected" : "pca_sTrackingL" });
        compEvent.fire();
    },
    
    openMyService : function(component, event, helper) {
     /*   var evt = $A.get("e.c:CWP_EventChangeComponent");
        evt.setParams({"toOpen": 'myService'});
        evt.fire();*/
        var compEvent =  $A.get("e.c:CWP_menu_Evt")
        compEvent.setParams({"tabSelected" : "myService" });
        compEvent.fire();
    },
    
    openIdeasCorner : function(component, event, helper) {
     /*   var evt = $A.get("e.c:CWP_EventChangeComponent");
        evt.setParams({"toOpen": 'ideasCorner'});
        evt.fire();*/
        var compEvent =  $A.get("e.c:CWP_menu_Evt")
        compEvent.setParams({"tabSelected" : "@@" });
        compEvent.fire();
    },
    
    openMyProfile : function(component, event, helper) {
       /* var evt = $A.get("e.c:CWP_EventChangeComponent");
        evt.setParams({"toOpen": 'profile'});
        evt.fire();*/
        
    },
    showSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        $A.util.removeClass(spinner,'slds-hide');
     },
    hideSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        $A.util.addClass(spinner,'slds-hide');
    },
    
    
})