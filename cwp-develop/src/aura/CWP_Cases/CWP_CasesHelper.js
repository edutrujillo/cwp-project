({
    getInicialTickets : function(component, evt,helper) {
        var action = component.get("c.getTickets");  
        action.setParams({ tableControllerS :'init' });
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                var returnValue = response.getReturnValue();
                var res = response.getReturnValue();
                component.set('v.ticketsList', JSON.parse(res["lisTickets"]));
                component.set('v.ticketsTableController', JSON.parse(res["offsetController"]));
            }
        });
        $A.enqueueAction(action);
    },
    getTickets : function(component, evt,helper, tableControl) {   
        var action = component.get("c.getTickets");    
        action.setParams({ tableControllerS :JSON.stringify(tableControl) });
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                var returnValue = response.getReturnValue();
                
                var res = response.getReturnValue();
                component.set('v.ticketsList', JSON.parse(res["lisTickets"]));
                component.set('v.ticketsTableController', JSON.parse(res["offsetController"]));
            }
        });
        $A.enqueueAction(action);
    }, 
    getFieldValues : function(component, evt,helper) {   
        var action = component.get("c.getFieldsValues");    
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                var returnValue = response.getReturnValue();
                debugger;
                
                var res = response.getReturnValue();
              component.set('v.caseTypeFields', JSON.parse(res["type"]));
                
                var a=  component.get('v.caseTypeFields');
            }
        });
        $A.enqueueAction(action);
    }
})