({
    doInit : function(component, event, helper) {   
        // 
        helper.getInicialTickets(component, event, helper);
    },
    
    nextPageTickets : function(component, event, helper) {
        debugger;
        //Calculamos el nuevo tableControl
        //
        var tableController= component.get('v.ticketsTableController');
        tableController.iniIndex+=tableController.viewSize;
        
        tableController.finIndex+=tableController.viewSize;
        if (!tableController.finIndex>tableController.numRecords)
            tableController.finIndex=tableController.numRecords;
        //Controlamos la paginación para la primera y la última página 
        if (tableController.iniIndex<=tableController.numRecords) 
            helper.getTickets(component, event, helper,tableController);
        
    },
    prevPageTickets : function(component, event, helper) {
        debugger;
        //Calculamos el nuevo tableControl
        var tableController= component.get('v.ticketsTableController');
        
        tableController.iniIndex-=tableController.viewSize;
        if (tableController.iniIndex>0 ){
            helper.getTickets(component, event, helper,tableController);
        }
    },
    changeSizeTickets : function(component, event, helper) {
        debugger;
        //Calculamos el nuevo tableControl
        var tableController= component.get('v.ticketsTableController');
        var e = document.getElementById("paginationTickets");
        var newSize = e.options[e.selectedIndex].value;
        tableController.viewSize=newSize;
        tableController.iniIndex=1;
        tableController.finIndex=newSize;
        helper.getTickets(component, event, helper,tableController);
        
    },
    advancedFilterTickets : function(component, event, helper) {
       
        helper.getFieldValues(component, event, helper);
        
        
    }
    
})