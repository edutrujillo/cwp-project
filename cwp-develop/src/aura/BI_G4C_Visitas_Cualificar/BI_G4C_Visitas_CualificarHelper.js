({
	avanzarVisita2: function(cmp, evt, helper){
        var evento = cmp.get('v.evento');
        var auxData = cmp.get('v.auxData');
        evento.BI_FVI_Estado__c = 'Cualificada';
        
        var action = cmp.get("c.updateEvent");
        action.setParams({
                e: JSON.stringify(evento),
                a: JSON.stringify(auxData),
            });
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                var resul = response.getReturnValue();                
                var ae = $A.get("e.c:BI_G4C_app_evt");
                ae.setParams({type:"finalizada"});
                ae.fire();
            }
            else{
                //console.log(response);
                //by JB 29/07
								//alert('error');
				        cmp.set('v.errorMessageHeader','Error');
				        cmp.set('v.errorMessageBody','Error');

				        var element = cmp.find('generic_error');
				        $A.util.removeClass(element, 'slds-hide');
            }
            var eventCualificada = $A.get("e.c:BI_G4C_Cualificada");
            eventCualificada.setParams({'event': evento});
            eventCualificada.fire();
        });
        $A.enqueueAction(action);
    }
})