({
    finalizar : function(cmp, evt, helper) {
        helper.avanzarVisita2(cmp, evt, helper);
    },
    closeError: function(cmp, evt, helper){
        var element = cmp.find('generic_error');
        $A.util.addClass(element,'slds-hide');
    }
})