({
	loadPins : function(component, event, helper) {       
        
       
        var acc=window.location.href;
        //alert(acc);
        var valor1=acc.length;
        var valor2=6; //Tamaño de los nombre de los parametros. 
        var longUrl=parseInt(valor1)-parseInt(valor2);
        console.log('Tamaño url: '+ longUrl);

        /*------Carga de los Id de la Oportunidad y de la ID de la cuenta*/
        var idAcc='';
        var idOpp='';

        for(var j=0;j<longUrl;j++){

            var word='';
            word=word.concat(acc[j+0]);
            word=word.concat(acc[j+1]);
            word=word.concat(acc[j+2]);
            word=word.concat(acc[j+3]);
            word=word.concat(acc[j+4]);
            word=word.concat(acc[j+5]);
            //console.log(word);

            if(word=='OptyId'){

                word='';
                word=word.concat(acc[j+7]);
                word=word.concat(acc[j+8]);
                word=word.concat(acc[j+9]);
                word=word.concat(acc[j+10]);
                word=word.concat(acc[j+11]);
                word=word.concat(acc[j+12]);
                word=word.concat(acc[j+13]);
                word=word.concat(acc[j+14]);
                word=word.concat(acc[j+15]);
                word=word.concat(acc[j+16]);
                word=word.concat(acc[j+17]);
                word=word.concat(acc[j+18]);
                word=word.concat(acc[j+19]);
                word=word.concat(acc[j+20]);
                word=word.concat(acc[j+21]);
               // word=word.concat(acc[j+16]);
                console.log(word);
                idOpp=word;
                component.set("v.idOportunidad",idOpp);


            }
            else if(word=='AcctId'){
               word='';
                word=word.concat(acc[j+7]);
                word=word.concat(acc[j+8]);
                word=word.concat(acc[j+9]);
                word=word.concat(acc[j+10]);
                word=word.concat(acc[j+11]);
                word=word.concat(acc[j+12]);
                word=word.concat(acc[j+13]);
                word=word.concat(acc[j+14]);
                word=word.concat(acc[j+15]);
                word=word.concat(acc[j+16]);
                word=word.concat(acc[j+17]);
                word=word.concat(acc[j+18]);
                word=word.concat(acc[j+19]);
                word=word.concat(acc[j+20]);
                word=word.concat(acc[j+21]);
               // word=word.concat(acc[j+16]);
                console.log(word);
                idAcc=word;
                component.set("v.idCuenta",idAcc);

            }

        }

         /*Carga de los nombre de la cuenta y de la oportunidad*/

         var actionAcc =component.get("c.getAccname");
         actionAcc.setParams({
            'varId': idAcc
         });
          actionAcc.setCallback(this,function(data){
            component.set("v.AccName", data.getReturnValue())            
        });
                      
        $A.enqueueAction(actionAcc); 

         var actionOpp =component.get("c.getOppname");
         actionOpp.setParams({
            'varId': idOpp
         });
          actionOpp.setCallback(this,function(data){
            component.set("v.OppName", data.getReturnValue())            
        });
                      
        $A.enqueueAction(actionOpp); 

        //console.log('Dir: '+ idAcc);
        
        /*-----Carga de la lista de sedes disponibles---------------------------*/
         var action = component.get("c.getInstallPoints");
        //console.log(window.location);       
        action.setParams({
            'accId': idAcc
        });
        action.setCallback(this,function(data){
            component.set("v.pins", data.getReturnValue())            
        });
                      
        $A.enqueueAction(action); 

        /*Cargar la lista de seleccionados*/
         var actionSelected = component.get("c.getListSelected");
        //console.log(window.location);       
        actionSelected.setParams({
            'accId' : idAcc, 'oppId' : idOpp
        });
        actionSelected.setCallback(this,function(data){
            component.set("v.lstSelected", data.getReturnValue())            
        });
                      
        $A.enqueueAction(actionSelected); 

         },


    
     redirect2Cart : function(component, event, helper){


        var long=component.get('v.lstSelected').length;

        if(long>0){
            console.log('Procesando la lista');

            var myListJS = component.get("v.lstSelected");
            console.log('saving now');
            console.log(myListJS);       

            var action = component.get("c.procesarSedes");
        
                action.setParams({ "lstSedes" : myListJS , "cuenta" : component.get("v.idCuenta") ,"oportunidad" : component.get("v.idOportunidad") });
        
                action.setCallback(this, function(response) {
            
                        var state = response.getState();
            
            if (component.isValid() && state === "SUCCESS") {   
                var result = response.getReturnValue();

                window.location.href = "/apex/NE__NewConfiguration?OptyId="+component.get("v.idOportunidad")+"&AcctId="+component.get("v.idCuenta")+"&oppId="+component.get("v.idOportunidad")+"&accId="+component.get("v.idCuenta");

                //sforce.one.navigatetourl('/NewConfiguration?OptyId='+component.get("v.idOportunidad")+'?AcctId='+component.get("v.idCuenta")
                 //window.location.href = "https://telefonicab2b--cotmexdev--ne.cs80.visual.force.com/apex/NewConfiguration?OptyId="+component.get("v.idOportunidad")+"?AcctId="+component.get("v.idCuenta");
                 //https://telefonicab2b--cotmexdev--ne.cs80.visual.force.com/apex/NewConfiguration?confCurrency=MXN&cSType=Completo&cType=New&accId=0012500000e9b3a&billAccId=0012500000e9b3a&oppId=00625000005Ycq1&servAccId=0012500000e9b3a
                }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }

                });
        
                     $A.enqueueAction(action);

        }else{
            
             window.location.href = "/apex/NE__NewConfiguration?OptyId="+component.get("v.idOportunidad")+"&AcctId="+component.get("v.idCuenta")+"&oppId="+component.get("v.idOportunidad")+"&accId="+component.get("v.idCuenta");

           // sforce.one.navigatetourl('/NewConfiguration?OptyId='+component.get("v.idOportunidad")+'?AcctId='+component.get("v.idCuenta")
          // window.location.href = "https://telefonicab2b--cotmexdev--ne.cs80.visual.force.com/apex/NewConfiguration?OptyId="+component.get("v.idOportunidad")+"?AcctId="+component.get("v.idCuenta");

        }

        
        //alert("Redireccionando");
        
        //window.location.href = "/apex/NewConfiguration";
        
        
        
    },

    back2Opp :  function(component, event, helper){
        
        //alert("Redireccionando");
        window.location.href = "/00625000005Ycq1AAC";
        //window.location.href = "/apex/NewConfiguration";
        
        
        
    },


   actionAfter : function(component, event, helper){

        
        var obj= event.getParam('pi_obj');
        //console.log('ID_OBJ: '+obj.Id);


        //var lstPi=component.get('v.pins');
        var long=component.get('v.lstSelected').length;
        var array=component.get('v.lstSelected');
        var encontrado=false;
        
       // console.log('Longitud_Lista1: '+long);

        // if(long>0)
        // {
        //     console.log('Valor id:'+ array[0].Id);
        // }
        

        for(var i=0;i<long;i++){

            console.log('Bucle');

            if(array[i].Id== obj.Id){

                //console.log('Entra siendo: '+encontrado);
                //console.log('ID ARRAY: '+array[i].Id);
                //console.log('Id  obj : '+obj.Id);

                encontrado=true;
                //console.log('Resultado: '+encontrado);
            }

        }

       // console.log('Encontrado: '+encontrado);

        if(encontrado == false){
            //console.log('Añadiendo');
            array.push(obj);
           component.set('v.lstSelected',array);
        }

        //console.log('Longitud--final: '+array.length);


    
        
        
    },

    removeSelected : function(component, event, helper){

        //alert("Borrando");


        var obj= event.getParam('ev_del_pi');
        //console.log('ID_OBJ: '+obj.Id);

        var lstSelected=component.get('v.lstSelected');
        var long=component.get('v.lstSelected').length;
        console.log('Tamaño lista:' + long);
        //var array=component.get('v.lstSelected');
        //var encontrado=false;

        for(var i=0; i<long;i++){
            console.log('Dentro del bucle de borrar');

            if(obj.Id==lstSelected[i].Id){

                // console.log(obj.Id);
                // console.log(lstSelected[i].Id);
                // console.log('Dnetro del if');

                lstSelected.splice(i,1);

                console.log('He borrado');
                
               component.set('v.lstSelected',lstSelected);
               console.log('Tamaño final selecionados:'+ lstSelected.length);

                //encontrado=true;
            }

        }


    }




})