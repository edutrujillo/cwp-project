({
	makeListSortable : function(component, event, helper, listToProcess) {
		var retWrapList = [];
        var caseAndAttachmentMap = new Object();
        var caseDetailMap = new Object();
        for(var i = 0 ; i<listToProcess.length; i++){
            var thereAttach = false;
            var attachList= [];
            if(listToProcess[i].Attachments != undefined && listToProcess[i].Attachments.length>0){
                for(var j = 0 ; j<listToProcess[i].Attachments.length; j++){
                    var innerAttachList = [listToProcess[i].Attachments[j].Id, listToProcess[i].Attachments[j].Name, listToProcess[i].Attachments[j].CreatedBy.Name, this.getFormattedDate(component, event, helper, listToProcess[i].Attachments[j].CreatedDate), '/servlet/servlet.FileDownload?file='+listToProcess[i].Attachments[j].Id];
                    attachList.push(innerAttachList);
                    thereAttach = true;
                }
                caseAndAttachmentMap[listToProcess[i].Id] = attachList;
            }
            var parent = '';
            if(listToProcess[i].BI2_PER_Caso_Padre__c!='0'){
                parent = listToProcess[i].BI2_PER_Caso_Padre__c;
            }
            var innerCaseList = [parent,
                                 listToProcess[i].CaseNumber,
                                 listToProcess[i].Reason,
                                 listToProcess[i].BI_COL_Fecha_Radicacion__c,
                                 listToProcess[i].Subject,
                                 listToProcess[i].BI2_Usuario_creador_de_la_incidencia__c,
                                 listToProcess[i].Status,
                                 listToProcess[i].Origin,
           						 listToProcess[i].Priority,
           						 listToProcess[i].Id,
                                 thereAttach,
                                 listToProcess[i].Description,
                                 listToProcess[i].TGS_Resolution__c];
            
            caseDetailMap[listToProcess[i].Id]=innerCaseList;
            retWrapList.push(innerCaseList);
        }
        component.set("v.originalCaseList", listToProcess);
        component.set("v.caseAndAttachmentMap", caseAndAttachmentMap);
        component.set("v.caseDetailMap", caseDetailMap);
        component.set("v.caseList", retWrapList);//lista completa
        return retWrapList;
        
	},
    
    upload1: function(component, event, helper, file, fileContents) {
        var action = component.get("c.saveTheFile"); 
 		var parentId = component.get("v.caseToShow")[9];
        alert(parentId);
        
        action.setParams({
            parentId: String(parentId),
            fileName: String(file.name),
            base64Data: String(encodeURIComponent(fileContents)), 
            contentType: String(file.type)
        });
 		//alert(action);
        action.setCallback(this, function(a){
            //alert('dentro de la llamada 1');
            var attachId = a.getReturnValue();
            //alert('dentro de la llamada 2');
            console.log(attachId);
        });
        //alert('llamada');
        $A.enqueueAction(action);   
        //alert('fin de la llamada');
        /*$A.run(function() {
            $A.enqueueAction(action); 
        });*/
    },
    
    setHeader : function(component, event, helper, fieldOrderMap){
        var orderFieldMap = new Object();
        var selectOptionList = [];
        var headerList = [];
        for(var i in fieldOrderMap){
            selectOptionList.push(i);
            headerList.push(i);
        }
        selectOptionList.splice(headerList.length-1, 1);
        component.set("v.headerList", headerList);
        component.set("v.selectOptionList", selectOptionList);
        component.set("v.columnMap", fieldOrderMap);
    },
    
    getFormattedDate : function(component, event, helper, dateToFormat){
        var creationDate = new Date(dateToFormat);
        var theMonth = creationDate.getMonth()+1;
        var theHour = creationDate.getHours();
        if(theHour<10){
            theHour = '0' + theHour;
        }
        var theMinutes = creationDate.getMinutes();
        if(theMinutes<10){
            theMinutes = '0' + theMinutes;
        }
        var theSeconds = creationDate.getSeconds();
        if(theSeconds<10){
            theSeconds = '0' + theSeconds;
        }
        var retDate = creationDate.getDate() + '/' + theMonth + '/' + creationDate.getFullYear() + ' ' + theHour + ':' + theMinutes + ':' + theSeconds;
        return retDate;
    },
    
    getPaginatedList : function(component, event, helper, listToProcess, index0, indexN, avance) {
        var retList=[];
        indexN = Math.min(indexN, listToProcess.length);
        for(var i = index0; i<indexN; i++){
            retList.push(listToProcess[i]);
        }
        debugger;
        var infoList = String(index0+Math.min(1, listToProcess.length)) + " - " + String(indexN) + ' Out of ' + String(listToProcess.length);
        component.set("v.infoList", infoList); //string con los datos de qué elementos están viendo, (1 a 10 de 118 elementos)
        //component.set("v.caseList", listToProcess);//lista completa
        component.set("v.inicio", index0);//índice del primer elemento que se muestra respecto a la tabla global
        component.set("v.fin", indexN); //indice del último elemento que se muestra respecto a la tabla global
        component.set("v.avance", avance); //nuevo avance seleccionado
        component.set("v.listSize", listToProcess.length); //tamaño de la lista global
        component.set("v.caseListToShow", retList); //lista para mostrar (la que se pinta en el iterador)
	},
    
    getOrderedList : function(component, event, helper, fieldToOrder, direction){
        var switching, i, x, y, shouldSwitch;
    	var table = component.get("v.caseList");//document.getElementById('assetTable');
        
        var columnToSort =[];
        var mapToOrder = new Object();
		for(var i =0; i<table.length; i++){
            if(mapToOrder.hasOwnProperty(table[i][fieldToOrder])){
                mapToOrder[table[i][fieldToOrder]].push(i);
            }else{
                var innerList =[i];
                mapToOrder[table[i][fieldToOrder]] = innerList;
            }
        }
        columnToSort = Object.keys(mapToOrder);
        
        columnToSort.sort();
        var inicio;
        var fin;
        var orderedList = [];
        debugger;
        if(direction==="DESC"){
            for(var i =columnToSort.length-1; i>=0; i--){
                for(var j = 0; j< mapToOrder[columnToSort[i]].length; j++){
                    orderedList.push(table[mapToOrder[columnToSort[i]][j]]);
                }
            }
        }else if(direction==="ASC"){
            for(var i =0; i< columnToSort.length; i++){
                for(var j = 0; j< mapToOrder[columnToSort[i]].length; j++){
                    orderedList.push(table[mapToOrder[columnToSort[i]][j]]);
                }
            }
        }
        this.getPaginatedList(component, event, helper, orderedList, 0, component.get("v.avance"), component.get("v.avance"));
    },
})