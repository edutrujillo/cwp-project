({
	  getGroups : function(component) {
        
        var action = component.get("c.getArchiviedGroups");    
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.resul_groups", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
     
})