({	
    openModal : function(cmp, evt) {
    	debugger;
        var idContact = evt.getParam("idContact");
        
        var action = cmp.get("c.getloadInfo");
        action.setParams({'idContact':idContact});
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                cmp.set("v.cont", response.getReturnValue());
                
                var element = cmp.find('cwp_modal');
                $A.util.removeClass(element, 'slds-hide');
            }
        });
        $A.enqueueAction(action);
        
    },
    hideModal : function(cmp) {
        var element = cmp.find('cwp_modal');
        $A.util.addClass(element, 'slds-hide');
    },
    executeEvent : function() {
        var idContact;
        idContact = '0032500000Z4HPUAA3';
        var modal = $A.get("e.c:CWP_EditContactModalEvent");
        modal.setParams({"idContact": idContact});    
        modal.fire();
    },
})