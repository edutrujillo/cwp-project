({
    getMembers : function(component) {
        
        var action = component.get("c.getGroupMembers");    
        //Callback
        action.setParams({"idgroup": component.get("v.resul_IDGroup")}); 
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
                var returnValue = actionResult.getReturnValue();
                if(returnValue != null){
                    alert('Miembros: '+ actionResult.getReturnValue());
                    component.set("v.resul_Members", actionResult.getReturnValue());
                }
            }
        });
        $A.enqueueAction(action);  
    },
})