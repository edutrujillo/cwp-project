({
    openModal : function(cmp, evt) {
        debugger;
        cmp.set("v.data",'');
        var id = evt.getParam("id");
        var type = evt.getParam("type");
        var back = evt.getParam("back");
        var clase = evt.getParam("clase");
        
        var action = cmp.get("c.getloadInfo");
        
        action.setParams({'id':id,'type':type,'back':back,'clase':clase});
        
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                cmp.set("v.data", response.getReturnValue());
                debugger;
                var element = cmp.find('cwp_modal');
                $A.util.removeClass(element, 'slds-hide');
            }
        });
        $A.enqueueAction(action);
        
        var actionLabel = cmp.get("c.getLabels");
        actionLabel.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                cmp.set("v.labelsObjectType", response.getReturnValue());
            }
        });
        $A.enqueueAction(actionLabel);
        
    },
    hideModal : function(cmp) {
        var element = cmp.find('cwp_modal');
        $A.util.addClass(element, 'slds-hide');
    },
    correo : function(cmp, evt, helper) {
        var idContact = cmp.get("v.data").UserId;
        var modal = $A.get("e.c:CWP_SendInfoModalEvent");
        modal.setParams({"idContact": idContact, "isButton": true});
        modal.fire();
        
        var element = cmp.find('cwp_modal');
        $A.util.addClass(element, 'slds-hide');
    },
    cita : function(cmp, evt, helper) {
        alert('Boton Cita');
    },
    perfil : function(cmp, evt, helper) {
        alert('Boton Perfil');
    },
    
    // eliminar cuando se implemente
    executeEvent : function() {
        var id;
        var type;
        var back;
        var clase;
        
        //Datos de prueba
        //id=00525000001fHkXAAU&clase=Equipo
        id = '00525000001fHkXAAU';
        clase='Equipo';
        var modal = $A.get("e.c:CWP_EquipoTelefonicaModalEvent");
        modal.setParams({"id": id, "back":back});    
        modal.fire();
    },
    modalAction: function(cmp, evt, helper) {
        var element = cmp.find('cwp_modal');
        debugger;
        // si el valor es true mostramos/ocultamos la modal 
        if(evt.getParam("showModal")){
            $A.util.removeClass(element, 'slds-hide');
        } else {
            $A.util.addClass(element, 'slds-hide');
        }  
    },
    
    
    
})