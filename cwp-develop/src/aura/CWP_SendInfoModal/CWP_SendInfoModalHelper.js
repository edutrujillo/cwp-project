({
	hideModal : function(cmp) {
		var element = cmp.find('cwp_modal');
        $A.util.addClass(element, 'slds-hide');
        
        if(cmp.get("v.isButton")) {
            var modal = $A.get("e.c:CWP_Show_EquipoTelefonicaModalEvent");
            modal.setParams({"showModal": true});    
            modal.fire();
        }
	}
})