({
    openModal : function(cmp, evt, helper) {
        
        // si el valor es true mostramos/ocultamos la modal
        cmp.set("v.isButton", evt.getParam("isButton"));
        
        var idParam = evt.getParam("idContact");
        
        var action = cmp.get("c.getloadInfo");
        action.setParams({'idParam':idParam});
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                debugger;
                cmp.set("v.us", response.getReturnValue());
                
                var element = cmp.find('cwp_modal');
                $A.util.removeClass(element, 'slds-hide');
            }
        });
        $A.enqueueAction(action);
        
        var action2 = cmp.get("c.getMail");
        action2.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                cmp.set("v.mail", response.getReturnValue());
            }
        });
        $A.enqueueAction(action2); 
    },
    hideModal : function(cmp, evt, helper) {
        helper.hideModal(cmp);
    },
    send : function(cmp,evt, helper) { 
        
        var user = cmp.get('v.us');
		var bodyMsjId   = cmp.find('bodyMsjId').getElement().value;
        var subjectId   = cmp.find('subjectId').getElement().value;
        var mailFrom = cmp.get('v.mail');
        var mailTo = cmp.get('v.us.Email');
       var action = cmp.get("c.getSend");
       action.setParams({'emailTo':mailTo,
                         'emailFrom': mailFrom,
                         'emailSubject':subjectId,
           				'emailBody':bodyMsjId
                         });
        
         action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                alert('Enviado correctamente');
                helper.hideModal(cmp);
            }
        });
        $A.enqueueAction(action);
    },
    executeEvent : function() {
        
        var idContact;
        idContact='00525000001fHkXAAU';
        var modal = $A.get("e.c:CWP_SendInfoModalEvent");
        modal.setParams({"idContact": idContact});    
        modal.fire();
    },
    initTest : function() {
        var id;
        var type;
        var back;
        var clase;
        
        //Datos de prueba
        //id=00525000001fHkXAAU&clase=Equipo
        id = '00525000001fHkXAAU';
        clase='Equipo';
        var modal = $A.get("e.c:CWP_EquipoTelefonicaModalEvent");
        modal.setParams({"id": id, "back":back,"isButton": true});    
        modal.fire();
    },
})