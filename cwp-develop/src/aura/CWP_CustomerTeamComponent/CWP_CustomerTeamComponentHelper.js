({
	getHeaders : function(component) {
        
        var action = component.get("c.getHeaders");     
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
            		console.log(actionResult.getReturnValue());
        			component.set("v.fieldSetRecords", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    
    getRecords : function(component) {
        
        var action = component.get("c.getRecords"); 
        //action.setParams({ "ident" : component.get("v.ident")});
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.fieldSet", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    
    indice : function(component) {
        
        var action = component.get("c.getIndice");    
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.index", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    numRecords : function(component) {
        
        var action = component.get("c.getNumRecords");    
        var dynamicCmp = component.find("InputSelectDynamic");
        component.set("v.numRecords",dynamicCmp.get("v.value"));
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.total", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    
    
    defineViews: function(component) {  
        var action = component.get("c.getVista");    
        var dynamicCmp = component.find("InputSelectDynamic");
        action.setParams({ "page" : dynamicCmp.get("v.value")});
        
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
                if(returnValue != null){
        			component.set("v.fieldSet", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    
    pages: function(component) {  
        var action = component.get("c.getPages");    
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.pages", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    onClick: function(component) {  
        var action = component.get("c.getNext");   
        var dynamicCmp = component.find("InputSelectDynamic");
        action.setParams({ "indice" : component.get("v.index"), "numRec" : component.get("v.numRecords"), "page" : dynamicCmp.get("v.value")  });
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.fieldSet", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    onClickBack: function(component) {  
        var action = component.get("c.getBack");  
        var dynamicCmp = component.find("InputSelectDynamic");
        action.setParams({ "indice" : component.get("v.index"), "numRec" : component.get("v.numRecords"), "page" : dynamicCmp.get("v.value")  });
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.fieldSet", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    ini: function(component) {  
        var action = component.get("c.getAttr"); 
        var dynamicCmp = component.find("InputSelectDynamic");
        action.setParams({ "indice" : component.get("v.index"), "numRec" : component.get("v.numRecords"), "page" : dynamicCmp.get("v.value") });
        
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			//component.set("v.fieldSet", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    getsearchFields : function(component){
        var action = component.get("c.defineSearch");
        var list=component.get("v.reasons");
         action.setParams({ "pickValues" : list});
        //Callback    	        
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {                
                var returnValue = response.getReturnValue();
                if(returnValue != null){                                                            
                    component.set("v.searchFields", returnValue);                    
                }
            }
        });
        $A.enqueueAction(action);                
    }
})