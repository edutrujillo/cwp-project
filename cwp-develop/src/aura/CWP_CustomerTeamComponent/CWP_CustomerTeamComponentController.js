({
    doInit : function(component, event, helper) { 
      
            var action = component.get("c.getIdent");    
            action.setCallback(this, function(actionResult) {
                if(component.isValid() && actionResult.getState() == "SUCCESS"){
                    var returnValue = actionResult.getReturnValue();
                    if(returnValue != null){
                        component.set("v.ident", actionResult.getReturnValue());
                    }
                }
            });
            $A.enqueueAction(action);
            
            helper.getHeaders(component); 
            helper.getRecords(component);
            helper.defineViews(component);
            helper.pages(component);
            helper.getsearchFields(component);
            
            // Initialize input select options
            var opts = [
                { "class": "optionClass", label: "5", value: "5"},
                { "class": "optionClass", label: "10", value: "10", selected: "true" },
                { "class": "optionClass", label: "25", value: "25" },
                { "class": "optionClass", label: "50", value: "50" },
                { "class": "optionClass", label: "100", value: "100" },
                { "class": "optionClass", label: "200", value: "200" }];
            component.find("InputSelectDynamic").set("v.options", opts);
            
            helper.indice(component);
            helper.numRecords(component);
    },
    
    
    defineViews: function(component, event, helper) {
        helper.defineViews(component);
    },
    
    onChange: function(component, event, helper) {
        helper.defineViews(component);
        helper.pages(component);
        helper.indice(component);
        helper.numRecords(component); 
    },
    onClick: function(component, event, helper) {   
        helper.ini(component);   
        helper.onClick(component); 
        helper.indice(component);
        helper.numRecords(component);
    },
    onClickBack: function(component, event, helper) {   
        helper.ini(component);   
        helper.onClickBack(component); 
        helper.indice(component);
        helper.numRecords(component);
    },
    hideModalCustomer: function(component, event, helper) {   
        document.getElementById("CWP_TeamComponent_div").style.display = "none";
    },
    showModalCustomer : function(component, event, helper) {
        document.getElementById("CWP_TeamComponent_div").style.display = "block";
    },
    saveCase: function(component, event, helper) {
        debugger;
        var checkFields=true;
        var reason=component.find('reason').get('v.value');
        var subject=component.find('newIdeaTitle').get('v.value');
        var description=component.find('description').get('v.value');
        var purpose=document.getElementById("horaInicio").value;
        
        if(reason===undefined || reason==='TODO JAAG' ){
            checkFields=false;
        }else if(subject===undefined || subject==='' ){
            checkFields=false;
        }else if(description===undefined || description==='' ){
            checkFields=false;
        }else if(purpose===undefined || purpose==='' ){
            checkFields=false;
        }
        if (checkFields){
            //var subject=document.getElementById("subject").value;
            //var description=document.getElementById("description").value;
            var action = component.get("c.setCase"); 
            //Callback
            action.setParams({
                "status": "Assigned",
                "reason": reason,
                "subject": subject,
                "description": description,
                "purpose": purpose
            }); 
            action.setCallback(this, function(actionResult) {
                if(component.isValid() && actionResult.getState() == "SUCCESS"){
                    var returnValue = actionResult.getReturnValue();
                    if(returnValue != null){
                        //alert(returnValue);
                    }
                }
            });
            $A.enqueueAction(action);
            document.getElementById("CWP_TeamComponent_div").style.display = "block";
        }
        else {
            
            alert('Revise los campos');
        }
        
    }, 
    
    openDetailContact : function(component, event, helper){
        debugger;
        var contactos = component.get("v.fieldSet");
        var contact = contactos[event.currentTarget.dataset.index];
        console.log(contact);
        var modal = $A.get("e.c:CWP_EditContactModalEvent");
        modal.setParams({"idContact": contact.Id});    
        modal.fire();
    },
    
})