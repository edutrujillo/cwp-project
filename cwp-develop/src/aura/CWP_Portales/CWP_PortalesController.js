({
	doInit : function(component, event, helper) {
        //pca_portales
        var tabSelected= event.getParam('tabSelected');
        if(tabSelected=== 'pca_portales'){
			helper.loadPortals(component);	
            var comp = component.find('CWP_Portales_div');
            $A.util.removeClass(component, 'slds-hide');
        }else {
            var comp = component.find('CWP_Portales_div');
            $A.util.addClass(component, 'slds-hide');
        }
	}
})