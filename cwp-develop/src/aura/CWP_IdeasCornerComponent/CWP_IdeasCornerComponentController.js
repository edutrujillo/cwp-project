({
	doInit : function(component, event, helper) {
		helper.getLastNews(component);
		helper.getImagesCarousel(component);
		helper.getSectorialNews(component);
        helper.getPopularIdeas(component);
        helper.getRecentIdeas(component);
        helper.getMyIdeas(component);
        helper.getFAQs(component);
	}, 
	
	mostrarMas : function(component){
		var limiteIdeas = component.get("v.ideasNumber");
		limiteIdeas += 5;
		component.set("v.ideasNumber", limiteIdeas);
	},
	
	reiniciarLimit : function(component){
		var limiteIdeas = component.get("v.ideasNumber");
		limiteIdeas = 5;
		component.set("v.ideasNumber", limiteIdeas);
	},
	
	ordenarCarrousel : function(component, event, helper){
		var elementos = document.getElementsByClassName("tlf-carousel carousel_wrapper");
		if(document.getElementsByClassName("tlf-carousel carousel_wrapper").length>0){
			
            //document.getElementsByClassName("tlf-carousel carousel_wrapper").CloudCarousel();
    	}
	},
    
    
    
    getPopIdeaId : function(component, event, helper){
        debugger;
    	var popIdeas = component.get("v.listPopularIdeasWrapper");
    	var selectedIdea = popIdeas[event.currentTarget.dataset.index].id;
    	component.set("v.selectedIdea", selectedIdea);
    	component.set("v.detalleIdea", popIdeas[event.currentTarget.dataset.index]);
    	component.set("v.isIdea", true);
    	component.set("v.isFAQ", false);
    	helper.abrirDetalle(component, event, helper);
    },
    
    getRecIdeaId : function(component, event, helper){
    	var recIdeas = component.get("v.listRecentIdeasWrapper");
    	var selectedIdea = recIdeas[event.currentTarget.dataset.index].id;
    	component.set("v.detalleIdea", recIdeas[event.currentTarget.dataset.index]);
    	component.set("v.selectedIdea", selectedIdea);
    	component.set("v.isIdea", true);
    	component.set("v.isFAQ", false);
    	helper.abrirDetalle(component, event, helper);
    },
    
    getMyIdeaId : function(component, event, helper){
    	var myIdeas = component.get("v.listMyIdeasWrapper");
    	var selectedIdea = myIdeas[event.currentTarget.dataset.index].id;
    	component.set("v.detalleIdea", myIdeas[event.currentTarget.dataset.index]);
    	component.set("v.selectedIdea", selectedIdea);
    	component.set("v.isIdea", true);
    	component.set("v.isFAQ", false);
    	helper.abrirDetalle(component, event, helper);
    },
    
    getFAQId : function(component, event, helper){
    	var faqs = component.get("v.solutionsPlatino");
    	component.set("v.detalleFAQ", faqs[event.currentTarget.dataset.index]);
    	component.set("v.isFAQ", true);
    	component.set("v.isIdea", false);
    	helper.abrirDetalle(component, event, helper);
    },
    
    showMoreFAQs : function(component, event, helper){
    	var faqs = component.get("v.solutionsPlatino");
    	component.set("v.detalleFAQ", faqs[0]);
    	component.set("v.isFAQ", true);
    	component.set("v.isIdea", false);
    	helper.abrirDetalle(component, event, helper);
    },
    
    ready : function(component, event, helper){
    	var readyMap = component.get("v.mapToReady");
    	readyMap[event.getParam("keyToMap")] = true;
    	component.set("v.mapToReady", readyMap);
    	var isReady = true;
    	var key;
    	for(key in readyMap){
    		if(readyMap[key] === false){
    			isReady = false;
    		}
    	}
    	//alert('isReady? ' + isReady);
    	if(isReady){
	    	setTimeout(function(){
	            if($("[data-function='data-selectpicker']").length>0){
	                $("[data-function='data-selectpicker']").selectpicker();
	            }
	            if($("[data-function='data-scroller']").length>0){
	                $("[data-function='data-scroller']").carousel({
	                    interval: 3000
	                });
	            }
	            if($("[data-function='data-carousel']").length>0){
	                $("[data-function='data-carousel']").CloudCarousel( {
	                    reflHeight: 56,
	                    reflGap:2,
	                    titleBox: $("[data-function='data-carousel__title']"),
	                    altBox: $("[data-function='data-carousel__subTitle']"),
	                    buttonLeft: $("[data-function='data-carousel__buttonLeft']"),
	                    buttonRight: $("[data-function='data-carousel__buttonRight']"),
	                    yRadius:40,
	                    xPos: 331,
	                    yPos: 33,
	                    speed:0.15,
	                    mouseWheel:false,
	                    carouselRadius:0.5
	                });
	            }
	            if($("[data-function='data-carousel--mobile']").length>0){
	                if(window.innerWidth>='480' && window.innerWidth<'768'){
	                    $("[data-function='data-carousel--mobile']").slick({
	                        slidesToShow: 1,
	                        slidesToScroll: 1,
	                        autoplay: true,
	                        arrows:true,
	                        dots:true,
	                        autoplay:false,
	                        draggable:false
	                    });
	                    //  createAlt();
	                    //changeAlt();
	                }
	                else{
	                    if ($("[data-function='data-carousel--mobile']").hasClass("slick-initialized")) {
	                        $("[data-function='data-carousel--mobile']").slick("destroy");
	                        //deleteAlt();
	                    }
	                }
	            }
	        }, 1);
        }
    },
    
    callCreateNewIdea : function(component, event, helper){
    	var title = component.find("newIdeaTitle").get("v.value");
    	var body = component.find("newIdea").get("v.value");
    	var action = component.get("c.insertIdea");
    	action.setParams({
    		"title" : title,
    		"body" : body
    	});
    	//Callback
        action.setCallback(this, function(response) {
            debugger;
            if (component.isValid() && response.getState() === "SUCCESS") {
            	var returnValue = response.getReturnValue();
                if(returnValue !== null){
					$('#tlf-add').modal('toggle');
					component.find("newIdeaTitle").set("v.value", " ");
					component.find("newIdea").set("v.value", " ");
					helper.getPopularIdeas(component);
					helper.getRecentIdeas(component);
					helper.getMyIdeas(component);
				}
            }
        });
    	$A.enqueueAction(action);
    }
})