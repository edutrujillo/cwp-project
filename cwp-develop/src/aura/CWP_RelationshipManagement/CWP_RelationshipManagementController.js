({

    doInit : function(component, event) {
        var tabSelected= event.getParam('tabSelected');
        if(tabSelected=== 'relationship'){
            // Ejecutamos código que lanzaríamos en el doInit
            var comp = component.find('CWP_RelationshipManagement');
            $A.util.removeClass(comp, 'slds-hide');
            
        }else {
            var comp = component.find('CWP_RelationshipManagement');
            $A.util.addClass(comp, 'slds-hide');
        }
        if(tabSelected==='relationship'){
            var imgUrl;
            var mainInfo = component.get('c.getServiceTrackingInfo');
            component.set("v.title", '{!$Label.c.PCA_Tab_RelMan_menu}');
            mainInfo.setCallback(this, function(response){
                var state = response.getState();
                if(component.isValid() && state === "SUCCESS"){
                    var mainInfo = response.getReturnValue();
                    imgUrl= "url('/servlet/servlet.FileDownload?file=" + mainInfo['imgId']+ "')";
                    setTimeout(
                        function(){
                            if(mainInfo['title'] != undefined){
                                component.set("v.title", mainInfo['title']);
                            }
                            if(mainInfo['imgId'] != undefined){
                                document.getElementById('mainImageRM').style.backgroundImage = imgUrl;
                                document.getElementById('mainImageRM').style.padding="2.5rem 4.0625rem";
                                document.getElementById('mainImageRM').style.minHeight="21.875rem";
                                document.getElementById('mainImageRM').style.backgroundSize="contain"
                            }else{
                                document.getElementById('mainImageRM').className = "tlf-slide";     
                            }
                            //url(!$Resource.PCA_imgSlider + 'Relationship.png')
                    }, 1);
                }else{
                    alert("fallo!");
                }
            });
            $A.enqueueAction(mainInfo);
        }
    },
    
    changeTab : function(component, event, helper) {
        var evt = $A.get("e.c:CWP_menu_Evt");
    	var tabToOpen = event.target.id;
        evt.setParams({"tabSelected": tabToOpen});
        evt.fire();
    }

})