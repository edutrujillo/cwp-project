({
	atCheck : function(component, event, helper) {
		var username = component.get("v.username");
        var showBox = false;
        if(event.getParams().keyCode === 13 || event.getSource().getLocalId() === 'atCheckButton'){
            if(username != null){
                for (var i = 0; i< username.length; i++) {
                    if(username[i] === "@"){
                        showBox = true;
                        component.set("v.showPassBox",showBox);
                    }
                }   
                if(showBox === false){
                    window.location.href = "https://www.google.es";
                }   
            }
		}            
	},
    submitPass : function(component, event, helper){
        var password = component.get("v.pass");
        var username = component.get("v.username");
        password = helper.toBase64(password);
        username = helper.toBase64(username);
        helper.login(component, username, password);
    },
    forgotPassword : function(component, event, helper){
        var username = component.get("v.username");
        username = helper.toBase64(username);
        helper.forgotPass(component, username);
    }
})