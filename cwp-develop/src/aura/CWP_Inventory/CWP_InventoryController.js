({
	doInit : function(component, event, helper) {
        var tabSelected= event.getParam('tabSelected');
        if(tabSelected=== 'inventory'){
            // Ejecutamos código que lanzaríamos en el doInit
            var comp = component.find('CWP_Inventory');
            $A.util.removeClass(comp, 'slds-hide');
            
        }else {
            var comp = component.find('CWP_Inventory');
            $A.util.addClass(comp, 'slds-hide');
        }
        var index0 = component.get("v.inicio");
        var indexN = component.get("v.fin");
        var avance = component.get("v.avance");
        if(tabSelected==="inventory"){
            var assetList = component.get('c.getAssetList');
            assetList.setCallback(this, function(response){
                var state = response.getState();
                if(component.isValid() && state === "SUCCESS"){
                    var retList = response.getReturnValue();
                    var assetListToOrder = helper.getSortableList(component, event, helper, retList);
                    helper.getPaginatedList(component, event, helper, assetListToOrder, index0, indexN, avance);
					//component.set("v.assetListToOrder", assetListToOrder);
                }else{
                    alert("fallo!");
                }
            });
            $A.enqueueAction(assetList);
        }
    },
    
    getNewAvance : function(component, event, helper){
        var newAvance = document.getElementById('pagination').value;
        var retList = component.get("v.assetList");
        var indexN = Math.min(newAvance, retList.length);
        helper.getPaginatedList(component, event, helper, retList, 0, indexN, newAvance);
    },
    
    listPaginateForward : function(component, event, helper){
        var retList = component.get("v.assetList");
        var index0 = parseInt(component.get("v.inicio"));	
        var indexN = parseInt(component.get("v.fin"));	
        var avance = parseInt(component.get("v.avance"));
       // alert("inicio	" + index0+ "	fin	" + indexN + "	avance	" + avance);
        if(index0+avance<retList.length){
            index0 = index0 + avance;
            indexN = index0 + avance;
            indexN = Math.min(indexN, retList.length);
        //alert("inicio	" + index0+ "	fin	" + indexN + "	avance	" + avance);
        	helper.getPaginatedList(component, event, helper, retList, index0, indexN, avance);
        }
        
    },
    
    listPaginateBackward : function(component, event, helper){
        var retList = component.get("v.assetList");
        var index0 = parseInt(component.get("v.inicio"));	
        var indexN = parseInt(component.get("v.fin"));	
        var avance = parseInt(component.get("v.avance"));
        //alert("inicio	" + index0+ "	fin	" + indexN + "	avance	" + avance);
       /* if(indexN-avance>0){
            indexN = indexN - avance;
            index0 = indexN - avance;
            index0 = Math.max(index0, 0);
        	helper.getPaginatedList(component, event, helper, retList, index0, indexN, avance);
        }*/
        if(indexN-avance>0){
            indexN = index0;
            index0 = indexN - avance;
            index0 = Math.max(index0, 0);
        	helper.getPaginatedList(component, event, helper, retList, index0, indexN, avance);
        }
        
    },
    
    getFieldToOrderBy : function(component, event, helper){
        var fieldToOrder = event.target.id;
        var direction = component.get("v.direction");
        if(direction==="DESC"){
            direction="ASC";
        }else{
            direction="DESC"
        }
        component.set("v.direction", direction);
        helper.getOrderedList(component, event, helper, fieldToOrder, direction);
    }
    
})