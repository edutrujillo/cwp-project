({
    loadInfo : function(component){                
        var action = component.get("c.loadInfo");                
        action.setParams({
            "auxTabActive" :  component.get("v.tabActive"),
        });        
        //Callback
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();
                if(returnValue != null){					                    
                }
            }
        });
        $A.enqueueAction(action);                
    },     
    
    getHeader : function(component){                
        var action = component.get("c.defineRecords");        
        action.setParams({                       
            "auxSearchResult" : null,//component.find("selecciona").get("v.value"),                        
            "auxSearchText" : null
        });
        //alert('SEARCH RESULT' + component.find("selecciona").get("v.value") + 'SEARCH TEXT' + component.get("v.searchText") );
        //Callback           
        action.setCallback(this, function(response) {                    
            if (component.isValid() && response.getState() === "SUCCESS") { 
                var returnValue = response.getReturnValue();                
                if(returnValue != null){                                                            
                    component.set("v.listFacturacionHeader", returnValue);
                }
            }
        });
        $A.enqueueAction(action);                
    },
    
    getHeaderSearch : function(component){                
        var action = component.get("c.defineRecords");        
        action.setParams({                       
            "auxSearchResult" : document.getElementById('selecciona').value,
            "auxSearchText" : component.get("v.searchText")
        });        
        //alert('SEARCH RESULT' + searchResult +  'SEARCH TEXT' + component.get("v.searchText") );
        //Callback    	
        action.setCallback(this, function(response) {        
            if (component.isValid() && response.getState() === "SUCCESS") { 
                var returnValue = response.getReturnValue();
                if(returnValue != null){                        
                }
            }
        });
        $A.enqueueAction(action);                
    },
    
    
    getRecords : function(component, event, helper){                
        var action = component.get("c.defineViews");        
        //Callback
        action.setCallback(this, function(response) {            
            if (component.isValid() && response.getState() === "SUCCESS") {                
                var returnValue = response.getReturnValue();
                if(returnValue != null){                                        
                    returnValue = JSON.parse(returnValue);                    
                    var rowsArray = new Array();                    
                    for (var i = 0; i < returnValue.length; i++) {
                        var cellsArray = new Array();
                        
                        cellsArray.push(returnValue[i].id);	
                        
                        for (var j = 0; j < returnValue[i].values.length; j++) {                   	                            
                            cellsArray.push(returnValue[i].values[j].value);
                        }                           
                        rowsArray[i] = cellsArray;                        
                    }                  
                    returnValue = rowsArray;
                    
                    var index0 = component.get("v.inicio");                                                            
                    var indexN = component.get("v.fin");                    
                    var avance = component.get("v.avance");
                    
                    if (returnValue.length>avance){						
                        indexN = Math.max(indexN, avance);
                	}
                    this.getPaginatedList(component, event, helper, returnValue, index0, indexN, avance); //paginacion de la lista de listas                    
                }
            }
        });
        $A.enqueueAction(action);                
    },
    
    
    getsearchFields : function(component){                
        var action = component.get("c.defineSearch");
        //Callback    	        
        action.setCallback(this, function(response) {            
            if (component.isValid() && response.getState() === "SUCCESS") {                
                var returnValue = response.getReturnValue();
                if(returnValue != null){                                                            
                    component.set("v.searchFields", returnValue);                    
                }
            }
        });
        $A.enqueueAction(action);                
    },
    
    
    
    getLastModificationDate : function(component){                
        var action = component.get("c.getLastModification");                     
        action.setCallback(this, function(response) {            
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();
                if(returnValue != null){
                    component.set("v.lastModification", returnValue);
                }
            }
        });        
        $A.enqueueAction(action);                
    },    
    
    getLastModificationFactDate : function(component){                
        var action = component.get("c.getLastModificationFact");                     
        action.setCallback(this, function(response) {            
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();
                if(returnValue != null){
                    component.set("v.lastModificationFact", returnValue);
                }
            }
        });        
        $A.enqueueAction(action);                
    },
    
    
    callCreateDebtTask : function(component){                	    
        var action = component.get("c.createDebtTask");        	
        action.setParams({            			
            "FacturaId" : component.get("v.billingId")            
        });                
        action.setCallback(this, function(response) {            
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();                
                if(returnValue != null){                              
					document.getElementById('bodyModal1form').style.display="none";
					document.getElementById('bodyModal1Message').style.display="block";
                }
            }
        });        
        $A.enqueueAction(action);    
        
    },
    
    callCreateAccounStatusTask : function(component){               
        var action = component.get("c.createAccountStatusTask");        	
        action.setParams({            			
            "FacturacionId" : component.get("v.debtId")            
        });                
        action.setCallback(this, function(response) {            
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();                                
                if(returnValue != null){                              
                    document.getElementById('bodyModal2form').style.display="none";
					document.getElementById('bodyModal2Message').style.display="block";
                }
            }
        });        
        $A.enqueueAction(action);    
        
    },    
    
    callChangeTab1 : function(component){                	    
        var action = component.get("c.setTabActive");
        action.setParams({
            "auxTab" : 'tab1'
        });
        action.setCallback(this, function(response) {            
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();                                
                if(returnValue != null){                              
                    component.set("v.tabActive", returnValue);
                }
            }
        });        
        $A.enqueueAction(action);        
    },
    
    callChangeTab2 : function(component){                	    
        var action = component.get("c.setTabActive");
        action.setParams({
            "auxTab" : 'tab2'
        });
        action.setCallback(this, function(response) {            
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();                                
                if(returnValue != null){                              
                    component.set("v.tabActive", returnValue);
                }
            }
        });        
        $A.enqueueAction(action);        
    },
    
    
    /******/
    getPaginatedList : function(component, event, helper, listToProcess, index0, indexN, avance) {        
        var retList=[];
        indexN = Math.min(indexN, listToProcess.length);
        for(var i = index0; i<indexN; i++){
            retList.push(listToProcess[i]);
        }
        var infoList = String(index0+1) + " - " + String(indexN) + ' Out of ' + String(listToProcess.length);
        component.set("v.infoList", infoList); //string con los datos de qué elementos están viendo, (1 a 10 de 118 elementos)
        component.set("v.billingList", listToProcess);//lista completa
        component.set("v.inicio", index0);//índice del primer elemento que se muestra respecto a la tabla global
        component.set("v.fin", indexN); //indice del último elemento que se muestra respecto a la tabla global
        component.set("v.avance", avance); //nuevo avance seleccionado
        component.set("v.listSize", listToProcess.length); //tamaño de la lista global
        component.set("v.billingListToShow", retList); //lista para mostrar (la que se pinta en el iterador)
    },
    
    /*
    getOrderedList : function(component, event, helper, fieldToOrder, direction){
        var table = component.get("v.billingList");//document.getElementById('assetTable');
        
        var columnToSort =[];
        var mapToOrder = new Object();
        for(var i =0; i<table.length; i++){
            if(mapToOrder.hasOwnProperty(table[i][fieldToOrder])){
                mapToOrder[table[i][fieldToOrder]].push(i);
            }else{
                var innerList =[i];
                mapToOrder[table[i][fieldToOrder]] = innerList;
            }
        }
        columnToSort = Object.keys(mapToOrder);
        
        columnToSort.sort();
        var inicio;
        var fin;
        var orderedList = [];
        debugger;
        if(direction==="DESC"){
            for(var i =columnToSort.length-1; i>=0; i--){
                for(var j = 0; j< mapToOrder[columnToSort[i]].length; j++){
                    orderedList.push(table[mapToOrder[columnToSort[i]][j]]);
                }
            }
        }else if(direction==="ASC"){
            for(var i =0; i< columnToSort.length; i++){
                for(var j = 0; j< mapToOrder[columnToSort[i]].length; j++){
                    orderedList.push(table[mapToOrder[columnToSort[i]][j]]);
                }
            }
        }
        this.getPaginatedList(component, event, helper, orderedList, 0, component.get("v.avance"), component.get("v.avance"));
    },
    */

    getNewOrderedList : function(component, event, helper, columnTitle, direction){
        var action = component.get("c.Order"); 
        //alert('columnTitle: ' + columnTitle);
        action.setParams({
            "auxFieldOrder" : columnTitle,
            "auxMode": direction
        });
        //Callback
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();
                if(returnValue != null){					
                }
            }
        });
        $A.enqueueAction(action);                
        
    }    
    
    
}
 
 })