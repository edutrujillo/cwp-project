({
	doInit : function(component, event, helper) {
		debugger;
		component.set("v.product", event.getParam("product"));
		var product = component.get("v.product");
		console.log('producto: ' + product);
		var desc1Split = product.BI_Texto_descriptivo_P1__c.split(/\n/);
		component.set("v.desc1Split", desc1Split);
		var desc2Split = product.BI_Texto_descriptivo_P2__c.split(/\n/);
		component.set("v.desc2Split", desc2Split);
		var compEvent =  $A.get("e.c:CWP_menu_Evt")
        compEvent.setParams({"tabSelected" : 'CWP_Catalog_Producto_div' });
        compEvent.fire();
	},
	
	showComponent :  function(component, event, helper){
		var tabSelected= event.getParam('tabSelected');
        if(tabSelected=== 'CWP_Catalog_Producto_div'){
            var comp = component.find('CWP_Catalog_Producto_div');
            $A.util.removeClass(component, 'slds-hide');
        }else {
            var comp = component.find('CWP_Catalog_Producto_div');
            $A.util.addClass(component, 'slds-hide');
        }
	},
	
	askMoreInfo : function(component, event, helper){
		debugger;
		var prodId = component.get("v.product").Id;
		var product = component.get("v.product");
		var action = component.get("c.createTask");
		action.setParams({
			"prodId" : component.get("v.product").Id,
			"productJSON" : JSON.stringify(product)
		});
		//Callback
        action.setCallback(this, function(response) {
        debugger;
        	alert('response.getState():'  + response.getState());
            if (component.isValid() && response.getState() === "SUCCESS") {
            alert('callback del create');
            	/*var returnValue = response.getReturnValue();
                if(returnValue !== null){
					component.set("v.listPopularIdeasWrapper", returnValue);
				}*/
            }
        });
        $A.enqueueAction(action);
	}
})