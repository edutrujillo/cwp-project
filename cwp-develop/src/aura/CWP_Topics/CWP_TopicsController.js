({
    doInit : function(cmp, event, helper) {
        
        var action = cmp.get("c.getTopics");
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                cmp.set("v.resul_topics", response.getReturnValue());
                
            }
        });
        $A.enqueueAction(action);
        
    },
})