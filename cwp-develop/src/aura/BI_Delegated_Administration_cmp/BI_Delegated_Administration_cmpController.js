({
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Javier López 
    Company:       GIT
    Description:   Do Init to preCharge all needed fields
    
    IN:            9 String with the fields that are necessary to create a new User
    OUT:           Roles
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    26/11/2016              Javier López Andradas    Initial Version
    17/02/2017              Javier López Andradas    Sorting List and new Error Messajes
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	 doInit: function(component, evt, helper) {
    


    var action = component.get("c.getRoles");
    action.setParams({"prima":"prima"});
    action.setCallback(this, function(response) {
      console.log("Roles status "+ response.getState());
      if(component.isValid() && response.getState()==="SUCCESS"){
        var Roles = response.getReturnValue();
        var nullable = new Object();
        nullable.Id = null;
        nullable.Name = null;
        var list = [];
        list.push(nullable);
        for(var i = 0;i<Roles.length;i++){
              var rol = new Object();
              rol.Id = Roles[i].Id;
              rol.Name = Roles[i].Name;
              list.push(rol);
        }

        component.set("v.auxRole",list);
      }
    
    });
    $A.enqueueAction(action); 

    var action2 = component.get("c.getProfiles");
    action2.setParams({"dos":"dos"});
    action2.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS"){
        var Profiles = response.getReturnValue();
        var list2 = [];
        var nullable = new Object();
        nullable.Id = null;
        nullable.Name = null;
        list2.push(nullable);
        for(var j=0;j<Profiles.length;j++){
          var profile = new Object();
          profile.Id=Profiles[j].Id;
          profile.Name=Profiles[j].Name;
          list2.push(profile);
        }
        list2.sort(function(a,b){
          if(a.label>b.label){
            return 1;
          }else{
            return -1;  
          }
        });
      }
      component.set("v.auxProfile",list2);
    });
    $A.enqueueAction(action2); 

    var action3 = component.get("c.getPermisosValues");
    action3.setParams({"dos":"dos"});
    action3.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS"){
        var permisos = response.getReturnValue();
        var list2 = [];
        var nullable = new Object();
        nullable.value = null;
        nullable.label = null;
        list2.push(nullable);
        for(var j=0;j<permisos.length;j++){
          var permiso = new Object();
          permiso.value=permisos[j].value;
          permiso.label=permisos[j].label;
          list2.push(permiso);
        }
        list2.sort(function(a,b){
          if(a.label>b.label){
            return 1;
          }else{
            return -1;  
          }
        });
      }

      component.set("v.Permisos",list2);
    });
    $A.enqueueAction(action3); 

    var action4 = component.get("c.getPaisesValues");
    action4.setParams({"dos":"dos"});
    action4.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS"){
        var paises = response.getReturnValue();
        var list2 = [];
        var nullable = new Object();
        nullable.value = null;
        nullable.label = null;
        list2.push(nullable);
        for(var j=0;j<paises.length;j++){
          var pais = new Object();
          pais.value=paises[j].value;
          pais.label=paises[j].label;
          list2.push(pais);
        }
        list2.sort(function(a,b){
          if(a.label>b.label){
            return 1;
          }else{
            return -1;  
          }
        });
      }
      component.set("v.Paises",list2);
    });
    $A.enqueueAction(action4); 

    var list4 = []
    var manager =new Object();
    manager.NombreApellido = "--Ninguno--";
    manager.UserName="";
    list4.push(manager);
    component.set("v.Managers",list4);


    var action5 = component.get("c.getDivisas");
    action5.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS"){
        var divisa = response.getReturnValue();
        var list2 =[];
        var nullable = new Object();
        nullable.value = null;
        nullable.label = null;
        list2.push(nullable);
        for(var j=0;j<divisa.length;j++){
          var dv = new Object();
          dv.value=divisa[j].value;
          dv.label=divisa[j].value+" - "+divisa[j].label;
          list2.push(dv);
        }
        list2.sort(function(a,b){
          if(a.label>b.label){
            return 1;
          }else{
            return -1;  
          }
        });
      }
      component.set("v.Divisas",list2);
    });
    $A.enqueueAction(action5);


    var action6 = component.get("c.getLeguage");
    action6.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS"){
        var divisa = response.getReturnValue();
        var list2 =[];
        var nullable = new Object();
        nullable.value = null;
        nullable.label = null;
        list2.push(nullable);
        for(var j=0;j<divisa.length;j++){
          var dv = new Object();
          dv.value=divisa[j].value;
          dv.label=divisa[j].label;
          list2.push(dv);
        }
        list2.sort(function(a,b){
          if(a.label>b.label){
            return 1;
          }else{
            return -1;  
          }
        });
      }
      component.set("v.Lenguages",list2);
      component.find("Lenguages-ui").set("v.value","es");

    });
    $A.enqueueAction(action6);

    var action7 = component.get("c.regionalConfig");
    action7.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS"){
        var divisa = response.getReturnValue();
        var list2 =[];
        var nullable = new Object();
        nullable.value = null;
        nullable.label = null;
        list2.push(nullable);
        for(var j=0;j<divisa.length;j++){
          var dv = new Object();
          dv.value=divisa[j].value;
          dv.label=divisa[j].label;
          list2.push(dv);
        }
        list2.sort(function(a,b){
          if(a.label>b.label){
            return 1;
          }else{
            return -1;  
          }
        });
      }
      component.set("v.LocaleSidKeys",list2);
      component.find("LocaleSidKey-ui").set("v.value","es_ES");
    });
    $A.enqueueAction(action7);
         
    var action8 = component.get("c.timeZone");
    action8.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS"){
        var divisa = response.getReturnValue();
        var list2 =[];
        var nullable = new Object();
        nullable.value = null;
        nullable.label = null;
        list2.push(nullable);
        for(var j=0;j<divisa.length;j++){
          var dv = new Object();
          dv.value=divisa[j].value;
          dv.label=divisa[j].label;
          list2.push(dv);
        }
        list2.sort(function(a,b){
          if(a.label>b.label){
            return 1;
          }else{
            return -1;  
          }
        });
      }
      component.set("v.TimeZoneSidKeys",list2);
      component.find("TimeZoneSidKey-ui").set("v.value","Europe/Paris");
    });
    $A.enqueueAction(action8);
    
    var action9 = component.get("c.SegmentoRegional");
    action9.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS"){
        var segmentos = response.getReturnValue();
        var list2 =[];
        var nullable = new Object();
        nullable.value = null;
        nullable.label = null;
        list2.push(nullable);
        for(var j=0;j<segmentos.length;j++){
          var dv = new Object();
          dv.value=segmentos[j].value;
          dv.label=segmentos[j].label;
          list2.push(dv);
        }
        list2.sort(function(a,b){
          if(a.label>b.label){
            return 1;
          }else{
            return -1;  
          }
        });
      }
      component.set("v.Segmentos",list2);
    });
    $A.enqueueAction(action9);
    
    var action10 = component.get("c.emailCode");
    action10.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS"){
        var segmentos = response.getReturnValue();
        var list2 =[];
        var nullable = new Object();
        nullable.value=null;
        nullable.label=null;
        list2.push(nullable);
        for(var j=0;j<segmentos.length;j++){
          var dv = new Object();
          dv.value=segmentos[j].value;
          dv.label=segmentos[j].label;
          list2.push(dv);
        }
        list2.sort(function(a,b){
          if(a.label>b.label){
            return 1;
          }else{
            return -1;  
          }
        });
      }
      component.set("v.EmailCodes",list2);
      component.find("EmailCode-ui").set("v.value","ISO-8859-1");
    });
    $A.enqueueAction(action10);
    var action11 = component.get("c.getClasificacion");
    action11.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS"){
        var segmentos = response.getReturnValue();
        var list2 =[];
        var nullable = new Object();
        nullable.value = null;
        nullable.label = null;
        list2.push(nullable);
        for(var j=0;j<segmentos.length;j++){
          var dv = new Object();
          dv.value=segmentos[j].value;
          dv.label=segmentos[j].label;
          list2.push(dv);
        }
        list2.sort(function(a,b){
          if(a.label>b.label){
            return 1;
          }else{
            return -1;  
          }
        });
      }
      component.set("v.Clasificaciones",list2);

    });
    $A.enqueueAction(action11);


    component.set("v.Create","true");
    component.find("button").set("v.label","Create");

  },

  selectedRoleOrCountry: function(component,event,helper){
    helper.findManager(component);
  },
  fillAliasField: function(component,event,helper){
    var FName= component.find("FName-fd").get("v.value");
    var lName = component.find("lName-fd").get("v.value");
    if((FName!="undefined" && FName!=null) && (lName!="undefined" && lName!=null)){
      var out = "";
      for(var j = 0;j<4;j++){
        if(FName.charAt(j)!=null)
          out=out+FName.charAt(j);
      }
      for(var j =0;j<4;j++){
        if(lName.charAt(j)!=null)
          out=out+lName.charAt(j);
      }
      component.find("Alias-fd").set("v.value",out);
    }
  },

  checkUserName: function(component,event,helper){
    helper.checkUSerName(component);
  },

  fillUserName: function(component,event,helper){
    var email= component.find("email-input").get("v.value");
    if(component.get("v.Create")=="true"){
      component.find("UserName-input").set("v.value",email);
      helper.checkUSerName(component);
    }
  },
  darPermisos: function(component,event,helper){
    var per= component.find("allPer-ui").get("v.value");
    var allPermisos = component.get("v.allPermisos");
    var asigPer = component.get("v.selectPer");
    for(var i =0;i<allPermisos.length;i++){
      if(allPermisos[i].Id == per){

        asigPer.push(allPermisos[i]);
        allPermisos.splice(i);
      }
    }

    component.set("v.allPermisos",allPermisos);
    component.set("v.selectPer",asigPer);
  },
  quitarPermisos: function(component,event,helper){
    var per= component.find("TempPer-ui").get("v.value");
    var allPermisos = component.get("v.allPermisos");
    var asigPer = component.get("v.selectPer");
    for(var i =0;i<selectPer.length;i++){
      if(selectPer[i].Id == per){
        allPermisos.push(selectPer[i]);
        asigPer.splice(i);
      }
    }
    component.set("v.allPermisos",allPermisos);
    component.set("v.selectPer",asigPer);
  },
   asignarPermisos: function(component,event,helper){
    var list_Id=[];
    var asigPer = component.get("v.selectPer");
    for(var i =0;i<asigPer.length;i++){
      
        list_Id.push(asigPer[i].Id);
      
    }
    var action = component.get("c.setPermissionsSet");
    action.setParams({"IdPer":list_Id,"UserName":component.find("UserName-input").get("v.value")});
    action.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS"){
        if(response.getReturnValue()=='OK'){
          alert('Permisos del usuario actualizados correctamente');
        }else{
          alert('Error al actualizar los permisos del usuario:\n'+response.getReturnValue());
        }
      }
    });
    $A.enqueueAction(action);
  },
  cancel: function(component,event,helper){
       component.set("v.Create",true);
       component.find("email-input").set("v.value","");
       component.find("button").set("v.label","Crear");
       component.find("UserName-input").set("v.value","");
       component.find("Cargo-input").set("v.value","");
       component.find("Company-input").set("v.value","");
       component.find("Departament-input").set("v.value","");
       component.find("FName-fd").set("v.value","");
       component.find("lName-fd").set("v.value","");
       component.find("Alias-fd").set("v.value","");
       component.find("telefono-input").set("v.value","");
       component.find("movil-input").set("v.value","");
       component.find("fRole-ui").set("v.value","");
       component.find("fProfile-ui").set("v.value","");
       component.find("Paises-ui").set("v.value","");
       component.find("Permisos-ui").set("v.value","");
       component.find("Manager-ui").set("v.value","");
       component.find("Division-input").set("v.value","");
       component.find("TimeZoneSidKey-ui").set("v.value","");
       component.find("Lenguages-ui").set("v.value","");
       component.find("Divisas-ui").set("v.value","");
       component.find("Apodo-fd").set("v.value","");
       component.find("LocaleSidKey-ui").set("v.value","");
       component.find("Active").set("v.value",true);
       component.find("ResetPass").set("v.value",false);
       component.find("Service").set("v.value",false);
       component.find("Marketing").set("v.value",false);
       component.find("DNI-fd").set("v.value","");
       component.find("PERSTC-fd").set("v.value","");
       component.find("PERSMC-fd").set("v.value","");
       component.find("Segmento-ui").set("v.value","");
       component.find("Clasificacion-ui").set("v.value","");
       component.find("fProfile-ui").set("v.label","Perfil*");
       component.find("Paises-ui").set("v.label","Pais*");
       component.find("Permisos-ui").set("v.label","Permisos*")
       component.find("TimeZoneSidKey-ui").set("v.label","Zona horaria*");
       component.find("LocaleSidKey-ui").set("v.label","Configuración local*");
       component.find("Lenguages-ui").set("v.label","Idioma*");
       component.find("Divisas-ui").set("v.label","Divisa*");
       component.find("EmailCode-ui").set("v.label","Codificación email*");
       component.find("fRole-ui").set("v.label","Rol*");
       component.find("Clasificacion-ui").set("v.label","Clasificacion del Usuario *");
       $A.util.addClass(component.find("MessageActive"),"slds-hide");
       $A.util.addClass(component.find("Per-lab"),"slds-hide");
       $A.util.addClass(component.find("Per-lay"),"slds-hide");
  },
  createUpdate: function(component,event,helper){

    var create = component.get("v.Create");
    var email = component.find("email-input").get("v.value");
    var UsrName = component.find("UserName-input").get("v.value");
    var Title = component.find("Cargo-input").get("v.value");
    var Company = component.find("Company-input").get("v.value");
    var Departament = component.find("Departament-input").get("v.value");
    var fName = component.find("FName-fd").get("v.value");
    var lName = component.find("lName-fd").get("v.value");
    var Alias = component.find("Alias-fd").get("v.value");
    var telefono = component.find("telefono-input").get("v.value");
    var movil = component.find("movil-input").get("v.value");
    var RoleId = component.find("fRole-ui").get("v.value");
    var ProfileId = component.find("fProfile-ui").get("v.value");
    var Country = component.find("Paises-ui").get("v.value");
    var Permiso = component.find("Permisos-ui").get("v.value");
    var managerUsrName = component.find("Manager-ui").get("v.value");
    var Division = component.find("Division-input").get("v.value");
    var TimeZone = component.find("TimeZoneSidKey-ui").get("v.value");
    var Lenguage = component.find("Lenguages-ui").get("v.value");
    var Divisa = component.find("Divisas-ui").get("v.value");
    var Apodo = component.find("Apodo-fd").get("v.value");
    var TimeZoneLo = component.find("LocaleSidKey-ui").get("v.value");
    var isActive = component.find("Active").get("v.value");
    var resetPass =  component.find("ResetPass").get("v.value");
    var emailEncode =  component.find("EmailCode-ui").get("v.value");
    var DNI = component.find("DNI-fd").get("v.value");
    var PERSTC = component.find("PERSTC-fd").get("v.value");
    var PERSMC = component.find("PERSMC-fd").get("v.value");
    var Segmento = component.find("Segmento-ui").get("v.value");
    var service = component.find("Service").get("v.value");
    var marketing = component.find("Marketing").get("v.value");
    var clasficacion = component.find("Clasificacion-ui").get("v.value");
    if ((create=="true"  && (email==null || UsrName==null || lName==null || Alias==null || RoleId==null || ProfileId==null || Country==null || Permiso==null || TimeZone==null || Lenguage==null || Divisa==null || Apodo==null || TimeZoneLo==null || emailEncode==null || clasficacion==null)) || (create=="false" && UsrName==null)){
      alert("Hay algún campo obligatorio sin rellenar por favor asegúrese de que estan rellenos los campos con *");
    }else{
      var spinner = component.find('spinnerCharged');
      $A.util.removeClass(spinner,'slds-hide');
      if(create=="true"){
        var emailSpl = email.split("@");
        if(emailSpl.length > 2){
          alert("Fromato de email incorrecto, demasiados @");
        }else if(emailSpl.length == 1){
           $A.util.addClass(spinner,'slds-hide');
          alert("Fromato de email incorrecto, sin @");
        }else{
          var emailSpl2 = emailSpl[1].split(".");
          if(emailSpl2.length==1){
             $A.util.addClass(spinner,'slds-hide');
            alert("Formato de email incorrecto, sin extensión \".XXX\"");
          }else if(emailSpl2[0]==""){
             $A.util.addClass(spinner,'slds-hide');
            alert("El correo no tiene dominio");
          }else{
            var action = component.get("c.createUser");
            action.setParams({"email":email,"UsrName":UsrName,"Ttle":Title,"Company":Company,"Departament":Departament,"fName":fName,"lName":lName,"Alias":Alias,"telefono":telefono,"movil":movil,"RoleId":RoleId,"ProfileId":ProfileId,"Country":Country,"Permiso":Permiso,"managerUsrName":managerUsrName,"Division":Division,"TimeZone":TimeZone,"Lenguage":Lenguage,"Divisa":Divisa,"Apodo":Apodo,"TimeZoneLo":TimeZoneLo,"encodeEmail":emailEncode,"isActive":isActive,"resetPass":resetPass,"DNI":DNI,"PERSTC":PERSTC,"PERSMC":PERSMC,"Segmento":Segmento,"Service":service,"Marketing":marketing,"Clasificacion":clasficacion});
              console.log(action.getParams());
              var result;
    		action.setCallback(this,function(response){ 
    		if(component.isValid() && response.getState()==="SUCCESS"){
            	result = response.getReturnValue();
              if(result == "OK"){
                 $A.util.addClass(spinner,'slds-hide');
                alert("Usuario creado correctamente");
              }else{
                 $A.util.addClass(spinner,'slds-hide');
                alert("Fallo de creacion del usuario\n"+result);
              }
            }else if(response.getState()==="ERROR"){
                var errors = response.getError();
                if(errors){
                    if (errors[0] && errors[0].message) {
                       $A.util.addClass(spinner,'slds-hide');
                        alert("Fallo en la creación del usuario por favor revise el mensaje\n"+ errors[0].message);
                    }

                }
            }
            });
             $A.enqueueAction(action);
          }
        }

      }else{
        if(email==null){
          var actionUp=component.get("c.updateUser");
          actionUp.setParams({"email":email,"UsrName":UsrName,"Ttle":Title,"Company":Company,"Departament":Departament,"fName":fName,"lName":lName,"Alias":Alias,"telefono":telefono,"movil":movil,"RoleId":RoleId,"ProfileId":ProfileId,"Country":Country,"Permiso":Permiso,"managerUsrName":managerUsrName,"Division":Division,"TimeZone":TimeZone,"Lenguage":Lenguage,"Divisa":Divisa,"Apodo":Apodo,"TimeZoneLo":TimeZoneLo,"encodeEmail":emailEncode,"isActive":isActive,"resetPass":resetPass,"DNI":DNI,"PERSTC":PERSTC,"PERSMC":PERSMC,"Segmento":Segmento,"Service":service,"Marketing":marketing,"Clasificacion":clasficacion});
          var resultUp;
          actionUp.setCallback(this,function(response){ 
          if(component.isValid() && response.getState()==="SUCCESS"){
            resultUp = response.getReturnValue();
            if(resultUp == "OK"){
               $A.util.addClass(spinner,'slds-hide');
              alert("Usuario actualizado correctamente");
            }else{
               $A.util.addClass(spinner,'slds-hide');
              alert("Fallo de creacion del usuario\n"+resultUp);
            }
          }else if(response.getState()==="ERROR"){
            var errors = response.getError();
            if(errors){
              if (errors[0] && errors[0].message) {
                 $A.util.addClass(spinner,'slds-hide');
                alert("Fallo en la creación del usuario por favor revise el mensaje\n"+ errors[0].message);
              }
            }
          }
        });
          $A.enqueueAction(actionUp);
        }else{
          var emailSpl = email.split("@");
          if(emailSpl.length > 2){
             $A.util.addClass(spinner,'slds-hide');
          alert("Fromato de email incorrecto, demasiados @");
          }else if(emailSpl.length == 1){
             $A.util.addClass(spinner,'slds-hide');
            alert("Fromato de email incorrecto, sin @");
          }else{
            var emailSpl2 = emailSpl[1].split(".");
    
            if(emailSpl2.length==1){
               $A.util.addClass(spinner,'slds-hide');
              alert("Formato de email incorrecto, sin extensión \".XXX\"");
            }else if(emailSpl2[0]==""){
               $A.util.addClass(spinner,'slds-hide');
              alert("El correo no tiene dominio");
            }else{
              var actionUp2=component.get("c.updateUser");
              actionUp2.setParams({"email":email,"UsrName":UsrName,"Ttle":Title,"Company":Company,"Departament":Departament,"fName":fName,"lName":lName,"Alias":Alias,"telefono":telefono,"movil":movil,"RoleId":RoleId,"ProfileId":ProfileId,"Country":Country,"Permiso":Permiso,"managerUsrName":managerUsrName,"Division":Division,"TimeZone":TimeZone,"Lenguage":Lenguage,"Divisa":Divisa,"Apodo":Apodo,"TimeZoneLo":TimeZoneLo,"encodeEmail":emailEncode,"isActive":isActive,"resetPass":resetPass,"DNI":DNI,"PERSTC":PERSTC,"PERSMC":PERSMC,"Segmento":Segmento,"Service":service,"Marketing":marketing,"Clasificacion":clasficacion});
              var resultUp;
              actionUp2.setCallback(this,function(response){ 
              if(component.isValid() && response.getState()==="SUCCESS"){
                resultUp = response.getReturnValue();
                if(resultUp == "OK"){
                   $A.util.addClass(spinner,'slds-hide');
                  alert("Usuario actualizado correctamente");
                }else{
                   $A.util.addClass(spinner,'slds-hide');
                  alert("Fallo de creacion del usuario\n"+resultUp);
                }
              }else if(response.getState()==="ERROR"){
                var errors = response.getError();
                 $A.util.addClass(spinner,'slds-hide');
                if(errors){
                  if (errors[0] && errors[0].message) {
                    alert("Fallo en la creación del usuario por favor revise el mensaje\n"+ errors[0].message);
                  }
                }
              }
              });
              $A.enqueueAction(actionUp2);
            }
          }
        }
      }
    
    }
  },
  upPer:function(component,event,helper){
    //Aqui ira la parte de los permisos hacia el siste
  },
  darPer:function(component,event,helper){
    //Aqui ira la parte de los permisos en el front(dar)
  },
  quitPer:function(component,event,helper){
    //Aqui ira la parte de los permisos en el front(quitar)
  },
  createUpdateFile: function(component,event,helper){
   var file = component.find('file');
   var file2 = file.getElemet()[0]; 
   var linesCsv =  file2.split('\n');
   var users = {};
   var userNames =[];
   var posUserNames = -1;
   var posisActive = -1;
   var posPais = -1;
   var pos
   var MapPais
   var user = linesCsv[0].split(',');
   for (var j=0;j<user.length;j++){
     if(user[j]=='UserName') posUserNames=j;
     else if(user[j]== 'isActive') posisActive=j;
     else if(user[j]== 'Pais__c') posPais=j;
     else if(user[j]== 'Email' ) posemail= j;
     else if(user[j]== 'Title') posTitle= j;
     else if(user[j]== 'CompanyName') posCompany= j;
     else if(user[j]== 'Department') posDepartament= j;
     else if(user[j]== 'FirstName') posfName= j;
     else if(user[j]== 'LastName') poslName= j;
     else if(user[j]== 'Alias') posAlias= j;
     else if(user[j]== 'Phone') postelefono= j;
     else if(user[j]== 'MobilePhone') posmovil= j;
     else if(user[j]== 'UserRole') posRole = j;
     else if(user[j]== 'Profile') posProfile = j;
     else if(user[j]== 'BI_Permisos__c') posPermiso= j;
     else if(user[j]== 'Manager') posmanagerUsrName= j;
     else if(user[j]== 'Division') posDivision= j;
     else if(user[j]== 'TimeZoneSidKey') posTimeZone= j;
     else if(user[j]== 'LanguageLocaleKey') posLenguage= j;
     else if(user[j]== 'CurrencyIsoCode') posDivisa= j;
     else if(user[j]== 'CommunityNickName') posApodo= j;
     else if(user[j]== 'LocaleSidKey') posTimeZoneLo= j;
     else if(user[j]== 'resetPass') posresetPass= j;
     else if(user[j]== 'EmailEncodingKey') posemailEncode= j;
     else if(user[j]== 'BI_DNI_Per__c') posDNI= j;
     else if(user[j]== 'BI_Peru_STC_Per__c') posPERSTC= j;
     else if(user[j]== 'BI_Peru_SMC_Per__c') posPERSMC= j;
     else if(user[j]== 'BI_Segmento_regional__c') posSegmento= j;
     else if(user[j]== 'UserPermissionsSupportUser') posservice= j;
     else if(user[j]== 'UserPermissionsMarketingUser') posmarketing= j;
   }
//
  //if(posUserNames==-1 || posisActive==-1 || posPais==-1){
  //  alert('El archivo no tiene los campos obligatorios de UserName o de isActive o Pais__c por favor revise el csv');
  //}else{
  //  for(var i = 1;i<linesCsv.length;i++){
  //    user = linesCsv[i].split(',');
  //    for(var j=0;j<user.length;j++){
  //      userNames.push(user[posUserNames]);
  //      users[user[posUserNames]]=user;
  //    }
  //  }
  // Vamos a llamar el servicio Web para ver que usuarios tenemos en el sistema.
  //  var callChekUsers = component.get("c.checkUsersMassive");
  //  var usersReturns;
  //  callChekUsers.setParams({"userNames":userNames});
  //  callChekUsers.setCallback(this,function(response){
  //    if(component.isValid() && response.getState()==="SUCCESS"){
  //      usersReturns = response.getReturnValue();
  //    }
  //  });
  //  $A.enqueueAction(callChekUsers);
  //  if(usersReturns == null){//son todos nuevos
  //    var Paises={};
  //    var nomPaises=[];
  //    for(var i=0;userNames.length;i++){
  //        var pais = Paises[users[userNames[i]][posPais]];
  //        if(pais = null){
  //          Paises[users[userNames[i]][posPais]]=1;
  //          nomPaises.push(users[userNames[i]][posPais]);
  //        }
  //        else{
  //          Paises[users[userNames[i]][posPais]]=pais+1;
  //        }
  //    }
  //    component.set("v.paisesAux",nomPaises);
  //    helper.NumOfPaises(component);
  //    var lic = component.get("v.licencias");
  //    if(lic==null){
  //      alert('No existen licencias para el pais/es introducidos en el csv');
  //    }else if(lic.length!=nomPaises.length){
  //      alert('Hay algún pais que no tiene licencias, los paises disponibles son: '+nomPaises);
  //    }else{

  //    }

  //  }else{

  //  }
  //}

  }
})