({
	checkUSerName : function(component) {
	var UsName = component.find("UserName-input").get("v.value");
    var action = component.get("c.checkUserNameMethod");
    var create = new Object();
    action.setParams({"userName":UsName});
    action.setCallback(this,function(response){  
      if(component.isValid() && response.getState()==="SUCCESS"){
   
        if (response.getReturnValue() != null){
          create = response.getReturnValue();
          alert("¡ATENCIÓN! Ha introducido un usuario ya existente si continua se actualizará");
          component.set("v.Create","false");
          $A.util.removeClass(component.find("lNameNM-lab"),"slds-hide");
          $A.util.addClass(component.find("lName-lab"),"slds-hide");
          $A.util.removeClass(component.find("AliasNM-lab"),"slds-hide");
          $A.util.addClass(component.find("Alias-lab"),"slds-hide");
          $A.util.removeClass(component.find("EmailNM-lab"),"slds-hide");
          $A.util.addClass(component.find("Email-lab"),"slds-hide");
          $A.util.removeClass(component.find("ApodoNM-lab"),"slds-hide");
          $A.util.addClass(component.find("Apodo-lab"),"slds-hide");
          component.find("button").set("v.label","Update");
          component.find("fProfile-ui").set("v.label","Perfil");
          component.find("Paises-ui").set("v.label","Pais");
          component.find("Permisos-ui").set("v.label","Permisos");
          component.find("TimeZoneSidKey-ui").set("v.label","Zona horaria");
          component.find("LocaleSidKey-ui").set("v.label","Configuración local");
          component.find("Lenguages-ui").set("v.label","Idioma");
          component.find("Divisas-ui").set("v.label","Divisa");
          component.find("fRole-ui").set("v.label","Rol");
          component.find("EmailCode-ui").set("v.label","Codificación email");
          component.find("Clasificacion-ui").set("v.label","Clasificacion del Usuario");
          $A.util.removeClass(component.find("Per-lab"),"slds-hide");
          $A.util.removeClass(component.find("Per-lay"),"slds-hide");
          var lProfile;
          var UserNameGestor;
          var lRole;
          var eRoleId=create.UserRoleId;
          var eProfId = create.ProfileId;
          var lst_Rol = component.get("v.auxRole");
          var lst_Prof = component.get("v.auxProfile");
          for(var i=0;i<lst_Rol.length;i++){
            if(lst_Rol[i].Id == eRoleId){
              component.find("fRole-ui").set("v.value",lst_Rol[i].Id);
              break;
            }
          }
          for(var i=0;i<lst_Prof.length;i++){
            if(lst_Prof[i].Id == eProfId){
              component.find("fProfile-ui").set("v.value",lst_Prof[i].Id);
              break;
            }
          }
		    component.find("EmailCode-ui").set("v.value",create.EmailEncodingKey);
    	  component.find("DNI-fd").set("v.value",create.BI_DNI_Per__c);
    	  component.find("PERSTC-fd").set("v.value",create.BI_Peru_STC_Per__c);
    	  component.find("PERSMC-fd").set("v.value",create.BI_Peru_SMC_Per__c);
		      component.find("Segmento-ui").set("v.value",create.BI_Segmento_regional__c);
       	  component.find("email-input").set("v.value",create.Email);
       	  component.find("Cargo-input").set("v.value",create.Title);
       	  component.find("Company-input").set("v.value",create.CompanyName);
       	  component.find("Departament-input").set("v.value",create.Division);
       	  component.find("FName-fd").set("v.value",create.FirstName);
       	  component.find("lName-fd").set("v.value",create.LastName);
       	  component.find("Alias-fd").set("v.value",create.Alias);
       	  component.find("telefono-input").set("v.value",create.Phone);
       	  component.find("movil-input").set("v.value",create.MobilePhone);
       	  component.find("Paises-ui").set("v.value",create.Pais__c);
       	  component.find("Permisos-ui").set("v.value",create.BI_Permisos__c);
       	  component.find("Division-input").set("v.value",create.Department);
       	  component.find("TimeZoneSidKey-ui").set("v.value",create.TimeZoneSidKey);
       	  component.find("Lenguages-ui").set("v.value",create.LanguageLocaleKey);
       	  component.find("Divisas-ui").set("v.value",create.CurrencyIsoCode);
       	  component.find("Apodo-fd").set("v.value",create.CommunityNickName);
       	  component.find("LocaleSidKey-ui").set("v.value",create.LocaleSidKey);
       	  component.find("Active").set("v.value",create.IsActive);
       	  component.find("ResetPass").set("v.value",false);
       	  component.find("Service").set("v.value",create.UserPermissionsSupportUser);
       	  component.find("Marketing").set("v.value",create.UserPermissionsMarketingUser);
          component.find("Clasificacion-ui").set("v.value",create.BI_Clasificacion_de_Usuario__c);
           if(create.IsActive == false){
           $A.util.removeClass(component.find("MessageActive"),"slds-hide");
          }else{
            $A.util.addClass(component.find("MessageActive"),"slds-hide");
          }
          if(create.ManagerId!=null && create.ManagerId !='undefinied')
              component.set("v.ManagerId",create.ManagerId);
       
          this.findManager(component);
          var actionAllPer = component.get("c.getPermissionSetUser");
          actionAllPer.setParams({"user":UsName});
          actionAllPer.setCallback(this,function(response1){
            if(component.isValid() && response1.getState()==="SUCCESS"){
              var AllPer=response1.getReturnValue();
              var mapAll={};
              for(var i = 0;i<AllPer.length;i++){
                mapAll[AllPer[i].Id]=AllPer[i].Name;

              }
              component.set("v.selectPer",AllPer);
              var actionPerUser=component.get("c.getPermissionsSetsAll");
              actionPerUser.setCallback(this,function(response2){
                console.log(response2);
                if(component.isValid() && response2.getState()==="SUCCESS"){
                  var PerUser = response2.getReturnValue();
                  var lis_Per=[];
                  for(var i = 0;i<PerUser.length;i++){
                    console.log(PerUser[i].Name);
                    if(PerUser[i].Id in mapAll){
                    
                    }else{
                      console.log(PerUser[i]);
                      lis_Per.push(PerUser[i]);
                    }
                  }
                  component.set("v.allPermisos",lis_Per);
                }
              });
              $A.enqueueAction(actionPerUser);
            }
          });
          $A.enqueueAction(actionAllPer);
        }else{
          component.set("v.Create","true");
          component.find("button").set("v.label","Create");

          component.find("Active").set("v.value",true);
          $A.util.addClass(component.find("lNameNM-lab"),"slds-hide");
          $A.util.removeClass(component.find("lName-lab"),"slds-hide");
          $A.util.addClass(component.find("AliasNM-lab"),"slds-hide");
          $A.util.removeClass(component.find("Alias-lab"),"slds-hide");
          $A.util.addClass(component.find("EmailNM-lab"),"slds-hide");
          $A.util.removeClass(component.find("Email-lab"),"slds-hide");
          $A.util.addClass(component.find("ApodoNM-lab"),"slds-hide");
          $A.util.removeClass(component.find("Apodo-lab"),"slds-hide");
          component.find("fProfile-ui").set("v.label","Perfil*");
          component.find("Paises-ui").set("v.label","Pais*");
          component.find("Permisos-ui").set("v.label","Permisos*")
          component.find("TimeZoneSidKey-ui").set("v.label","Zona horaria*");
          component.find("LocaleSidKey-ui").set("v.label","Configuración local*");
          component.find("Lenguages-ui").set("v.label","Idioma*");
          component.find("Divisas-ui").set("v.label","Divisa*");
          component.find("EmailCode-ui").set("v.label","Codificación email*");
          component.find("fRole-ui").set("v.label","Rol*");
          component.find("Clasificacion-ui").set("v.label","Clasificacion del Usuario *");
          $A.util.addClass(component.find("MessageActive"),"slds-hide");
          $A.util.addClass(component.find("Per-lab"),"slds-hide");
          $A.util.addClass(component.find("Per-lay"),"slds-hide");
     	  var apodo = UsName.split("@");
     	  component.find("Apodo-fd").set("v.value",apodo[0]);
        } 
      }

    });
      $A.enqueueAction(action);

	},

	findManager: function(component){
		var Role = component.find("fRole-ui").get("v.value");
    	var country = component.find("Paises-ui").get("v.value");
    	if((Role!="undefinied" && Role!=null) && (country!="undefinied" && country!=null)){
    	  var action5 = component.get("c.getManagers");
    	  action5.setParams({"prima":Role,"seconda":country});
    	  action5.setCallback(this,function(response){
    	    if(component.isValid() && response.getState()==="SUCCESS"){
    	      var managers = response.getReturnValue();
    	      var list = [];
    	      var none = new Object();
    	      none.NombreApellido = "--Ninguno--";
    	      none.UserName = "";
    	      list.push(none);
    	      for(var j=0;j<managers.length;j++){
    	        var manager = new Object();
    	        manager.NombreApellido = managers[j].FirstName+" "+managers[j].LastName;
    	        manager.Id = managers[j].Id;
    	        list.push(manager);
    	      }
    	    }
    	    component.set("v.Managers",list);
          for(var j=0;j<managers.length;j++){
            if(managers[j].Id == component.get("v.ManagerId")){
              component.find("Manager-ui").set("v.value",managers[j].Id);
              break;
            }
          }
    
    	  });
    	  $A.enqueueAction(action5);
    	}
	},
  NumOfPaises: function(component){
    var callOutPaises = component.get("c.numberLicensesMassive");
    callOutPaises.setParams({"paises":component.get("v.paisesAux")});
    callOutPaises.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS")
        component.set("v.licencias",response.getReturnValue());
    });
    $A.enqueueAction(callOutPaises);
  }
})