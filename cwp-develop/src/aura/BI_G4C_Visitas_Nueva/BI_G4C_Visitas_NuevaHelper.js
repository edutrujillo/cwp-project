({
    getDataValue : function(cmp,accountId) {
        var action = cmp.get("c.getContactData");
        action.setParams({acctId:accountId});     
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {    
                var todos = JSON.parse(response.getReturnValue()); 
                var contactos = [];
                var oportunidades = [];
                var indexcontact = 0;
                var indexoportunity = 0;
                for (var i = 0; i < todos.length; i++) { 
                    if (todos[i].tipo === "Contacto"){
                        contactos[indexcontact] = todos[i];
                        indexcontact++;
                    }else if (todos[i].tipo === "Oportunidad"){
                        oportunidades[indexoportunity] = todos[i];
                        indexoportunity++;
                    }
                }
                
                cmp.find("listaContacto").set("v.options", contactos);
                cmp.find("listaOportunidad").set("v.options", oportunidades);
                cmp.find("bInicial").set("v.value", true);
            }
            else{            	
                //alert('error');
                //console.log(response);
                //by JB
            }
        });
        
        $A.enqueueAction(action);
        
    },
    getAuxData : function(cmp) {
        var action = cmp.get("c.getContactData");
        action.setParams({acctId:cmp.get('v.evento').WhatId});     
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {    
                var todos = JSON.parse(response.getReturnValue()); 
                var contactos = [];
                var oportunidades = [];
                var indexcontact = 0;
                var indexoportunity = 0;
                for (var i = 0; i < todos.length; i++) { 
                    if (todos[i].tipo === "Contacto"){
                        contactos[indexcontact] = todos[i];
                        indexcontact++;
                    }
                    if (todos[i].tipo === "Oportunidad"){
                        oportunidades[indexoportunity] = todos[i];
                        indexoportunity++;
                    }
                }
                cmp.find("listaContacto").set("v.options", contactos);
                cmp.find("listaOportunidad").set("v.options", oportunidades);
            }
            else{            	
                //alert('error');
                //console.log(response);
                //by JB
            }
        });
        
        $A.enqueueAction(action);
        
    },
    getEventData : function(cmp, eventId) {
        //console.log('getEventData');						
        var action = cmp.get("c.getEventData");  
        action.setParams({eventId:eventId});     
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {            	
                var eventHelper = response.getReturnValue();                                    
                
                var eventHelperJson=JSON.parse(eventHelper);
                
                var contact = eventHelperJson.c;
                
                if (contact){
                    var auxData = cmp.get('v.auxData');
                    var opts = [{ class: "optionClass", label: contact.Name, value: contact.Id, selected: "true" } ];
                    
                    cmp.find("listaContacto").set("v.options", opts);
                    auxData.ContactId=contact.value;
                    auxData.BI_G4C_Oportunidad__c=eventHelperJson.o.Name;
                    auxData.BI_G4C_IdOportunidad__c=eventHelperJson.o.Id;
                    opts = [{ class: "optionClass", label: auxData.BI_G4C_Oportunidad__c, value: auxData.BI_G4C_IdOportunidad__c, selected: "true" } ];
                    cmp.find("listaOportunidad").set("v.options", opts);
                    auxData.ContactName=contact.label;
                    auxData.ContactPhone=contact.Phone;
                    auxData.ContactMail=contact.Email;
                    cmp.set('v.auxData', auxData);
                }  
               debugger;
                var evento = eventHelperJson.e;
              	var d=new Date(evento.ActivityDateTime);
			    var hors=d.getHours()>9?d.getHours():'0'+d.getHours();
                var mins=d.getMinutes()>9?d.getMinutes():'0'+d.getMinutes();
                cmp.set('v.evFecha',d.getDate()+'/'+(parseInt(d.getMonth())+1)+'/'+d.getFullYear());
                cmp.set('v.evHora', hors+':'+mins);
                
                if (evento.IsReminderSet===true){	
                    /* 
                    var s = evento.ActivityDateTime;
                    var a = s.split(/[^0-9]/);					
                    var d1=new Date (a[0],a[1]-1,a[2],a[3],a[4],a[5] );
                    s = evento.ReminderDateTime;
                    a = s.split(/[^0-9]/);
                    var d2=new Date (a[0],a[1]-1,a[2],a[3],a[4],a[5] );
                    var minis = d1-d2;		            
                    minis /= 60000;		            		           
                    cmp.set('v.auxData.avisar',true);
                    cmp.set('v.auxData.avisoEnMins',minis);*/
                }		        
                
                cmp.set('v.evento', evento);              
            }
            else{            	
                //alert('error');
                //console.log(response);
            }
        });
        $A.enqueueAction(action);
    },
    existeFecha: function(fecha){
        var result=false;
        var RegExPattern = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;
        if(fecha!== undefined){
            if ((fecha.match(RegExPattern)) && (fecha!='')) {
                var fechaf = fecha.split("/");
                var day = fechaf[0];
                var month = fechaf[1];
                var year = fechaf[2];
                var date = new Date(year,month,'0');
                if((day-0)>(date.getDate()-0)){
                    result=false;
                }
                result=true;
            }
        }
        return result;
    },
    
    compruebaFecha: function(fecha){
        var result=false;
        var RegExPattern = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;
        if(fecha!== undefined){
            if ((fecha.match(RegExPattern)) && (fecha!='')) {
                var fechaf = fecha.split("/");
                var d = fechaf[0];
                var m = fechaf[1];
                var y = fechaf[2];
                result= m > 0 && m < 13 && y > 0 && y < 32768 && d > 0 && d <= (new Date(y, m, 0)).getDate();
            }
        }
        return result;
    },
    compruebaHora: function(hora){
        
        var result=false;
        var RegExPattern = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;
        if(hora!==undefined){
            
            if ((hora.match(RegExPattern)) && (hora!='')) {
                
                var horaf = hora.split(":");
                var hh = horaf[0];
                var mm = horaf[1];
                result= hh >= 0 && hh < 24 && mm >= 0 && mm < 60;
            }
        }
        return result
    },
    
    fieldValidation: function(cmp, evt, helper){
        
        var resul = true;
        var evento = cmp.get('v.evento'); 
        var auxData = cmp.get('v.auxData');
        var evFecha=cmp.get('v.evFecha');
        var evHora=cmp.get('v.evHora');      
        
        if (!helper.existeFecha(evFecha) || !helper.compruebaFecha(evFecha)){
            //(evento.ActivityDateTime === undefined || evento.ActivityDateTime.length === 0){
            var fvisitamsg = cmp.find('fvisitamsg');
            $A.util.removeClass(fvisitamsg, 'slds-hide');
            resul = false;
        }
        if (!helper.compruebaHora(evHora)){
            var hvisitamsg = cmp.find('hvisitamsg');
            $A.util.removeClass(hvisitamsg, 'slds-hide');
            resul = false;
        }
        if(resul===true){
            evento.ActivityDateTime= evFecha +'@'+evHora;
            
        }
        
        if (evento.DurationInMinutes === undefined ||evento.DurationInMinutes === null || evento.DurationInMinutes.length === 0){
            var durationmsg = cmp.find('durationmsg');
            $A.util.removeClass(durationmsg, 'slds-hide');
            resul = false;
        }
        
        if (evento.Subject === undefined  || evento.Subject.length === 0){
            evento.Subject="Visita a Cliente";
            /*  var asuntomsg = cmp.find('asuntomsg');
            $A.util.removeClass(asuntomsg, 'slds-hide');
            resul = false;*/
        }
        
        /* if (auxData.avisar === true && (auxData.avisoEnMins ===undefined  || auxData.avisoEnMins.length === 0)){
            var avisomsg = cmp.find('avisomsg');
            $A.util.removeClass(avisomsg, 'slds-hide');
            resul = false;
        }*/
        if (cmp.find("listaOportunidad").get("v.value") === 'Este cliente no tiene oportunidades'){
            var descriptionmsg = cmp.find('opportunitymsg');
            $A.util.removeClass(descriptionmsg, 'slds-hide');
            resul = false;
        }
        
        if (cmp.find("listaContacto").get("v.value") === 'Este cliente no tiene contactos'){
            descriptionmsg = cmp.find('contactmsg');
            $A.util.removeClass(descriptionmsg, 'slds-hide');
            resul = false;
        }
        
        return resul;
    },
    removeAllErrors: function(cmp){
        var fvisitamsg = cmp.find('fvisitamsg');
        $A.util.addClass(fvisitamsg, 'slds-hide');
        var hvisitamsg = cmp.find('hvisitamsg');
        $A.util.addClass(hvisitamsg, 'slds-hide');
        var durationmsg = cmp.find('durationmsg');
        $A.util.addClass(durationmsg, 'slds-hide');
        var asuntomsg = cmp.find('asuntomsg');
        $A.util.addClass(asuntomsg, 'slds-hide');
        var descriptionmsg = cmp.find('descriptionmsg');
        $A.util.addClass(descriptionmsg, 'slds-hide');
        var avisomsg = cmp.find('avisomsg');
        $A.util.addClass(avisomsg, 'slds-hide');
    },
    showCualificar: function(cmp, evt, helper){
        debugger;
        helper.removeAllErrors(cmp);
        var pasaValidations = helper.fieldValidation(cmp, evt, helper);
        if (pasaValidations){
            var x = function(a){
                //console.log(a);
                //by JB
            }
            
            $A.createComponent(
                "c:BI_G4C_Visitas_Cualificar",
                {
                    evento: cmp.get('v.evento'),
                    auxData: cmp.get('v.auxData')
                },          
                function(newCmp,status){
                    var modalfilter = cmp.find('BI_G4C_Visitas_Cualificar_id');
                    $A.util.removeClass(modalfilter,'slds-hide');
                    $A.util.removeClass(modalfilter,'.slds-modal--large');
                    var body = cmp.get("v.body");
                    body.push(newCmp);
                    cmp.set("v.body", body);
                }
            );
            
        }
    },
    updateVisita: function(cmp, evt, helper){
       
        var evento = cmp.get('v.evento');
        var d=new Date(evento.ActivityDateTime);
        
        var ms=parseInt(d.getMonth(),10);
        ms+=1;
        evento.ActivityDateTime=d.getFullYear()+'-'+ms+'-'+d.getDate()+'T'+d.getHours()+':'+d.getMinutes()+':00.000';
        var auxData = cmp.get('v.auxData');
        var action = cmp.get("c.updateEvent");
        
        evento.BI_G4C_Oportunidad__c = cmp.find("listaOportunidad").get("v.value");
        action.setParams({e: JSON.stringify(evento), a: JSON.stringify(auxData)});
        action.setCallback(this, function(response) {
           
            if (cmp.isValid() && response.getState() === "SUCCESS") {           
                var result = response.getReturnValue();
                /*Sub-Evento*/
                var ae = $A.get("e.c:BI_G4C_app_evt");
                ae.setParams({type:"refresh"});
                ae.fire();
                
                if (result === 'OK') {
                    
                }
                else if (result === 'QA') helper.showCualificar(cmp, evt, helper);
            }else{
                //Mensaje de error "No se ha podido actualizar el evento"
                //alert('ERROR updateVisita'+response.getState());
            }
        });
        $A.enqueueAction(action);
    },
    
    moveVisita: function(cmp, evt, helper){
    
        var evento = cmp.get('v.evento');        
        if(!evento.BI_FVI_Estado__c) {
            evento.BI_FVI_Estado__c = 'Nueva';
        }else if(evento.BI_FVI_Estado__c === 'Planificada'){
            evento.BI_FVI_Estado__c = 'Iniciada';
        }else if (evento.BI_FVI_Estado__c ==='Iniciada'){
            evento.BI_FVI_Estado__c = 'Finalizada';
        }else if (evento.BI_FVI_Estado__c ==='Finalizada'){
            evento.BI_FVI_Estado__c = 'LLamaCualificar';
        }
        
        /*var d=new Date(evento.ActivityDateTime);
        var ms=parseInt(d.getMonth(),10);
        ms+=1;
        evento.ActivityDateTime=d.getFullYear()+'-'+ms+'-'+d.getDate()+'@'+d.getHours()+':'+d.getMinutes()+':00.000';*/
        var auxData = cmp.get('v.auxData');
        var action = cmp.get("c.updateEvent");
        
        evento.BI_G4C_Oportunidad__c = cmp.find("listaOportunidad").get("v.value");
        action.setParams({e: JSON.stringify(evento), a: JSON.stringify(auxData)});
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {           
                var result = response.getReturnValue();
                /*Sub-Evento*/
                if (result === 'QA') {
                    helper.showCualificar(cmp, evt, helper);
                }else{
                    var ae = $A.get("e.c:BI_G4C_app_evt");
                    ae.setParams({type:"refresh"});
                    ae.fire();
                }
            }else{
                //Mensaje de error "No se ha podido actualizar el evento"
                //alert('ERROR '+response.getState());
            }
        });
        $A.enqueueAction(action);
        cmp.set("v.evento", evento);      
    }
})