/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Miguel Molina Cruz
Company:       everis
Description:   JS Controller for the Lightning Component BI_G4C_Visitas_Nueva

History:


<Date>                          <Author>                    <Code>					<Change Description>
20/06/2016                      Miguel Molina Cruz          -						Initial version
26/07/2016                      Miguel Molina Cruz          M001					Update contact email and phone for states after "Planificada" in the doInit method
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
({
    doInit: function(cmp, evt, helper){
        var evento = cmp.get('v.evento');
        var creacionVisita = true;
        if ( evento.Id === undefined){
            helper.getAuxData(cmp);
            var action = cmp.get("c.datosCuenta");
            action.setParams({acctId:cmp.get('v.evento').WhatId });
            action.setCallback(this, function(response) {
                if (cmp.isValid() && response.getState() === "SUCCESS") {
                    if(creacionVisita){
                        var auxData = cmp.get('v.auxData');
                        var res = JSON.parse(response.getReturnValue());
                        auxData.ContactId=res[0].value;
                        auxData.ContactName=res[0].label;
                        
                        auxData.ContactPhone=res[0].phone;
                        auxData.ContactMail=res[0].email;
                        
                        auxData.BI_G4C_Oportunidad__c=res[0].OpName;
                        cmp.set('v.auxData', auxData);
                        
                    }
                }
                else{
                    //console.log(response);
                    //alert('error DO INIT' + response);
                    //by JB 29/07
                }
            });
            $A.enqueueAction(action);
        }
        else{
            creacionVisita = false;
            helper.getEventData(cmp, evento.Id);
        }
        
    },
    
    emailphone: function(cmp, evt, helper){
        
        var action = cmp.get("c.datosContacto");
        var aux=cmp.find("listaContacto").get('v.value');
        action.setParams({contactId:aux});
        //console.log(aux);
        //by JB 29/07
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                var auxData = cmp.get('v.auxData');
                var res = JSON.parse(response.getReturnValue());
                auxData.ContactId=res[0].value;
                auxData.ContactName=res[0].label;
                auxData.ContactPhone=res[0].phone;
                auxData.ContactMail=res[0].email;
                auxData.BI_G4C_Oportunidad__c=res[0].OpName;
                cmp.set('v.auxData', auxData);
                
            }
            else{
                //console.log(response);
                //alert('error');
                //by JB
            }
        });
        $A.enqueueAction(action);
    },
    
    cargaOpo: function(cmp, evt, helper){
        
        var auxData = cmp.get('v.auxData');
        var selectCmp = cmp.find('listaOportunidad');
        auxData.BI_G4C_Oportunidad__c = selectCmp.get("v.value");
        cmp.set('v.auxData', auxData);
    },
    test : function(cmp, evt, helper) {
        $A.get("e.c:G4C_app_evt").fire();
    },
    
    avanzarVisita: function(cmp, evt, helper){
        debugger;
        helper.removeAllErrors(cmp);
        if(helper.fieldValidation(cmp, evt, helper) === true){
        	helper.moveVisita(cmp, evt, helper); 
        }
    },
    
    editarVisita: function(cmp, evt, helper){
        debugger;
        var evento = cmp.get("v.evento");
        if(evento.BI_FVI_Estado__c === 'Planificada'){
            helper.removeAllErrors(cmp);
            if(helper.fieldValidation(cmp, evt, helper) === true){
                helper.updateVisita(cmp, evt, helper);
            }
        }else {
            var ae = $A.get("e.c:BI_G4C_app_evt");
            ae.setParams({type:"refresh"});
            ae.fire();
        } 
    },
    
    handleEvent: function(cmp, evt, helper){
        var type = evt.getParam('type');
        
        if (type==='finalizada'){
            var device = $A.get("$Browser.formFactor");
            if (device==='DESKTOP'){
                window.setTimeout(
                    $A.getCallback(function() {
                        $A.get('e.force:hidePanel').fire();
                    }), 500
                );
            }
            else{
                $A.get('e.force:hidePanel').fire();
            }
            
        }
    },
    
    closeError: function(cmp, evt, helper){
        var element = cmp.find('generic_error');
        $A.util.addClass(element,'slds-hide');
    },
    
    visitaCualificada: function(cmp, evt, helper){
        var evento = evt.getParam("event");
        cmp.set('v.evento', evento);
    },
    hideModalnewVisitas: function(cmp, evt, helper){
        cmp.set("v.body", '');
        var confirm = cmp.find('BI_G4C_Visitas_Cualificar_id');
        
        $A.util.addClass(confirm, 'slds-hide');
    }
})