({
    rerender: function (component, helper) {
        try{       
            var auxData = component.get('v.auxData');
            var evento = component.get('v.evento');
            
            //Save Data
            evento.ActivityDateTime = component.find("fechaEvento").get("v.value");
            evento.DurationInMinutes = component.find("duracionEvento").get("v.value");
            evento.Subject = component.find("asuntoEvento").get("v.value");
            evento.Description = component.find("descriptionEvento").get("v.value");
            component.set('v.evento', evento);
            
            //Print Data
            component.find("contactEmail").set("v.value", auxData.ContactMail);
            component.find("contactPhone").set("v.value", auxData.ContactPhone);
            component.find("descriptionEvento").set("v.value", evento.Description);
        }catch(err) {
            //alert(err.message);
        }
    }
})