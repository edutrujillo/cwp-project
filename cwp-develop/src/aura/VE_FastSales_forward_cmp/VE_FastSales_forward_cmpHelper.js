({
    forwardOppty : function(cmp, helper) {      

        var oppty = cmp.get('v.opportunity');
        var action = cmp.get("c.forwardOpportunity");
        action.setParams({
            o: oppty,
            tipoCierre: cmp.get('v.tipoCierre'),
            fechaRealCierre: cmp.get('v.fechaRealCierre')
        });

        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {               
                var fo = JSON.parse( response.getReturnValue() );
                //console.log('XXXX');  
                //console.log(fo);

                if ( fo.o == null ){

                    var cmpTarget = cmp.find('bclose');
                    $A.util.removeClass(cmpTarget, 'slds-hide'); 
                    $A.util.addClass(cmpTarget, 'slds-button slds-button--neutral');

//					Manuel Medina 2016-07-25 ServiceLock Correction
//                    window.opener.location = '/'+oppty.Id;

                    //alert('No se ha podido avanzar la oportunidad: '+fo.m);
                    var messages = cmp.get('v.messages');
                    var errorMsg = fo.m;

                    var i = errorMsg.indexOf('TION,');
                    errorMsg = errorMsg.substring(i+6);
                    i = errorMsg.indexOf('.:');
                    
                    if (i>0){
                        errorMsg = errorMsg.substring(0,i);                        
                    }
                    else{
                        i = errorMsg.indexOf(': []');
                        if (i>0){
                            errorMsg = errorMsg.substring(0,i);
                        }
                    }

                    var find = '&quot;';
                    var re = new RegExp(find, 'g');

                    errorMsg = errorMsg.replace(re, '');                    

                    messages.push('No se ha podido avanzar la oportunidad: '+errorMsg);
                    cmp.set('v.messages', messages);                                        
                }
                else{
                    var oo = cmp.get('v.opportunity');
                    var stageValues = cmp.get('v.stageValues');
                    var mapa = new Map();
                	for(var i = 0; i<stageValues.length;i++){
                    	mapa.set(stageValues[i].value, stageValues[i].label);
                	}
                    oo.StageName = fo.o.StageName;
                    if(fo.o.StageName == 'F5 - Solution Definition'||fo.o.StageName == 'F4 - Design Solution'||fo.o.StageName == 'F3 - Offer Presented'|| fo.o.StageName == 'F2 - Negotiation'||fo.o.StageName == 'F1 - Closed Won' || fo.o.StageName == 'F1 - Closed Lost'|| fo.o.StageName == 'F1 - Cancelled | Suspended'){
                        cmp.set("v.show", true);
                    }
                    else{
                        cmp.set("v.show", false);
                    }
                    cmp.set('v.opportunity', oo);
                    var messages = cmp.get('v.messages');
                    messages.push(mapa.get(fo.o.StageName + ''));
                    cmp.set('v.messages', messages);
                    

                    if ( fo.o.StageName != 'F1 - Closed Won' && fo.o.StageName != 'F1 - Closed Lost' && fo.o.StageName != 'F1 - Cancelled | Suspended'){
                        helper.forwardOppty(cmp, helper);
                    }
                }
            }
            else{
                console.log(response);
                alert(response.getState());
            }
        });
        $A.enqueueAction(action);
    }
})