({
    doInit : function(cmp, event, helper) {
    var str =window.location.href;
       // alert(str);
    /*var tabSelected= event.getParam('tabSelected');
        if(tabSelected=== 'pca_profilelightning'){*/

        
    //var displaying = event.getParam("toOpen");
    //if(displaying==="profile"){

        var action = cmp.get("c.getUserId");
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
            	cmp.set("v.Id", response.getReturnValue());
                //Línea para el iframe, posiblemente para eliminar
                //cmp.set("v.ifmsrc",'https://telefonicab2b--crqdev.lightning.force.com/one/one.app#/sObject/'+response.getReturnValue()+'/view?slideDevName=detail&a:t=1488293859291');		
            }
        });
        $A.enqueueAction(action);
        
        /*var action = cmp.get("c.getUserName");
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                debugger;
            	cmp.set("v.Name", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
        
        helper.getTeams(cmp);
        helper.getFile(cmp);
        helper.getGroup(cmp);
        helper.getFollower(cmp);
        helper.getFollow(cmp);
     
    //}
    /*	var comp = component.find('CWP_ProfileLightning_div');
        $A.util.removeClass(component, 'slds-hide');

	}else {
         var comp = component.find('CWP_ProfileLightning_div');
         $A.util.addClass(component, 'slds-hide');
    }*/

	},
    setOutput : function(component, event, helper) {
        var idx = event.target.id;
        component.set("v.Id", idx);
	},

    updateRecordView: function(component, event, helper) {
        var id = component.get("v.Id");
        if(id!=null){
        var container = component.find("container");
        $A.createComponent("force:recordView",
                           {recordId: id,type: "MINI"},
                           function(cmp) {
                               container.set("v.body", [cmp]);
                           });
            
        var container2 = component.find("container2");
         $A.createComponent("force:recordView",
                           {recordId: id},
                           function(cmp) {
                               container2.set("v.body", [cmp]);
                           });
            
        /*var containerfeed = component.find("containerfeed");
         $A.createComponent("forceChatter:feed",
                            {type:"UserProfile", feedDesign:"DEFAULT"},
                           function(cmp) {
                               containerfeed.set("v.body", [cmp]);
                           });*/
        var action = component.get("c.getUserName");
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                debugger;
            	component.set("v.Name", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
        
        helper.getTeams(component);
        helper.getFile(component);
        helper.getGroup(component);
        helper.getFollower(component);
        helper.getFollow(component);
        
        }
     },
    
    save : function(component, event, helper) {
		component.find("edit").get("e.recordSave").fire();
        document.getElementById("modal").style.display = "none" ;
	},
    onSaveSuccess : function(component, event, helper) {
        var id = component.get("v.Id");
        if(id!=null){
        var container = component.find("container");
        $A.createComponent("force:recordView",
                           {recordId: id,type: "MINI"},
                           function(cmp) {
                               container.set("v.body", [cmp]);
                           });
        var container2 = component.find("container2");
         $A.createComponent("force:recordView",
                           {recordId: id},
                           function(cmp) {
                               container2.set("v.body", [cmp]);
                           });
        var containerfeed = component.find("containerfeed");
         $A.createComponent("forceChatter:feed",
                            {type:"UserProfile", feedDesign:"DEFAULT"},
                           function(cmp) {
                               containerfeed.set("v.body", [cmp]);
                           });
        var action = component.get("c.getUserName");
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                debugger;
            	component.set("v.Name", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
        
        helper.getTeams(component);
        helper.getFile(component);
        helper.getGroup(component);
        helper.getFollower(component);
        helper.getFollow(component);
        }    
	},
    showModal : function(component, event, helper) {
       document.getElementById("modal").style.display = "block";
    },
    hideModal : function(component,event, helper){
       document.getElementById("modal").style.display = "none";
   	},
    
	openFile : function(component, event, helper) {

        var idx = event.target.id; 
        
        /*var evt = $A.get("e.c:CWP_EventProfile");
		evt.setParams({ "result": idx});
		evt.fire();
        */
     	var evt = $A.get("e.c:CWP_EventProfile");
        evt.setParams({"toOpen": 'file', "Id": idx});
        evt.fire();
    },
        
    openGroup : function(component, event, helper) {

        var idx = event.target.id; 
     	var evt = $A.get("e.c:CWP_EventProfile");
        //alert('ID group:'+idx);
        evt.setParams({"toOpen": 'group', "Id": idx});
        evt.fire();
    },
        
})