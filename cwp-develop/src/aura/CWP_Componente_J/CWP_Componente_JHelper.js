({
	getTeams : function(component) {
        
        var action = component.get("c.getEquipo");    
        //Callback
		action.setParams({"identif": component.get("v.Id")});        
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.fieldSet", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    getFile : function(component) {
        
        var action = component.get("c.getArch");    
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.chatterfiles", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    getGroup : function(component) {
        
        var action = component.get("c.getGroups");    
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.group", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    getFollow : function(component) {
        
        var action = component.get("c.getFollowing");    
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.following", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    getFollower: function(component) {
        
        var action = component.get("c.getFollowers");    
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.followers", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    
})