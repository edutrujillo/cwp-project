({
    openModal : function(cmp, evt, helper) {
        helper.resetAll(cmp);
        helper.getLoadInfo(cmp, evt, helper);
    },
    hideModal : function(cmp) {
        var element = cmp.find('cwp_modal');
        $A.util.addClass(element, 'slds-hide');
    },
    save: function(cmp, evt, helper) {
                debugger;
        var data = cmp.get('v.data');
        var eventId = data.eventId;

        var fecha = data.fecha;
        
        var selectedUser = '';
        var sel = cmp.get('v.selectedUser');
        for(var i = 0; sel.length > i; i++) {
            var temp = sel[i].split(',');
              selectedUser =  selectedUser+temp[0]+';';      
        }
        
        var initHour = cmp.find("hourInitId").get("v.value");
        var finishHour = cmp.find("hourFinId").get("v.value");
        var webDate = data.day;
        
        var mensajeError = 'No ha seleccionado ningún usuario';
        
        if(selectedUser != '') {      
            var action = cmp.get("c.getSaveOrUpdate");
            
            action.setParams({'eventId':eventId,'fecha':fecha,'selectedUser':selectedUser,'initHour':initHour,'finishHour':finishHour, 'webDate' : webDate});
            
            action.setCallback(this, function(response) {
                if (response.getState() === "SUCCESS") {
                    response.getReturnValue();
                    // reload page
                }
            });
            $A.enqueueAction(action);
        }  
    },
    deleteEvent: function(cmp, evt, helper) {
        
    },
    add: function(cmp, evt, helper) {
        var allUsers = 'allUser';
        var selectedUsers = 'selectedUser';
        
        helper.addRemove(cmp,allUsers,selectedUsers);
        
    },
    remove: function(cmp, evt, helper) {
        var allUsers = 'allUser';
        var selectedUsers = 'selectedUser';
        
        helper.addRemove(cmp,selectedUsers,allUsers);
    },
    // eliminar cuando se implemente
    executeEvent : function() {

        var id;
        var pastDay;
        var day;
        
        id='00U25000003BeGEEA0';
        pastDay=false;
        day='Tue Mar 14 00:00:00 GMT 2017';
        var modal = $A.get("e.c:CWP_CustomEventDetailModalEvent");
        modal.setParams({"id": id,"pastDay": pastDay,"day": day});    
        modal.fire();
    },
})