({
    getLoadInfo : function(cmp, evt,helper ) {
        var eventId = evt.getParam("id");
        var pastDay = evt.getParam("pastDay");
        var day = evt.getParam("day");
        
        var action = cmp.get("c.getLoadInfo");
        
        action.setParams({'eventId':eventId,'pastDay':pastDay,'day':day});
        
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                cmp.set('v.data',response.getReturnValue());
                helper.getSelectedUser(cmp, evt);
                helper.getUsers(cmp, evt);
                helper.getEvent(cmp, evt);
                helper.getHours(cmp, evt);
            }
        });
        $A.enqueueAction(action);
    },
    getEvent : function(cmp, evt) {
        var data = cmp.get('v.data');
        var eventId = data.eventId;
        
        var action = cmp.get("c.getEvent");
        
        action.setParams({'eventId':eventId});
        
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                response.getReturnValue();   
                console.log('Event'+response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    getUsers : function(cmp, evt) {
        var data = cmp.get('v.data');
        var eventId = data.eventId;
        
        var action = cmp.get("c.getUsers");
        
        action.setParams({'eventId':eventId});
        
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") { 
                cmp.set('v.allUser',response.getReturnValue());
                var datos = response.getReturnValue();
                var opts = this.dynamicOptions(datos);
                console.log('Users'+response.getReturnValue());
                cmp.find("allUserId").set("v.options", opts);
            }
        });
        $A.enqueueAction(action);
    },
    getSelectedUser : function(cmp, evt) {
        var data = cmp.get('v.data');
        var eventId = data.eventId;
        
        var action = cmp.get("c.getSelectedUser");
        
        action.setParams({'eventId':eventId});
        
        action.setCallback(this, function(response) {
            console.log('Selected User'+response.getState());
            if (response.getState() === "SUCCESS") {
                cmp.set('v.selectedUser',response.getReturnValue());
                console.log('Selected User'+response.getReturnValue());
                var datos = response.getReturnValue();
                var opts = this.dynamicOptions(datos);
                
                cmp.find("selectedUserId").set("v.options", opts);
            }
        });
        $A.enqueueAction(action);
    }, 
    getHours : function(cmp, evt) {
        
        var action = cmp.get("c.getHours");
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                cmp.set('v.hoursList',response.getReturnValue());
                var data = cmp.get('v.data');
                console.log('Hours'+response.getReturnValue());
                cmp.find('hourInitId').set("v.value",data.initHour );
                cmp.find('hourFinId').set("v.value",data.finishHour );
                
                var element = cmp.find('cwp_modal');
                $A.util.removeClass(element, 'slds-hide'); 
            } 
        });
        $A.enqueueAction(action);
    },
    dynamicOptions : function(datos) {
        var opts = [];
        for(var i = 0; datos.length > i; i++){
            var temp = datos[i].split(',');
            var add = { label: temp[1], value: temp[0] };
            opts.push(add);
        }
        return opts;
    },
    resetAll : function(cmp) {
        
        cmp.set('v.data', null);
		cmp.set('v.hoursList', null);
        cmp.set('v.allUser', null);
        cmp.set('v.selectedUser', null);
        cmp.set('v.evnt', null);
        cmp.set('v.data', null);
        
        cmp.find('selectedUserId').set("v.options", []);
        cmp.find('allUserId').set("v.options", []);
        
        cmp.find('descriptionId').set("v.value", '');
    },
    addRemove : function(cmp, IdRemove, IdAdd) {
        
        var allUsers = cmp.get('v.'+IdRemove);
        var selectedUsers = cmp.get('v.'+IdAdd);
        
        var values = cmp.find(IdRemove+"Id").get("v.value");
        
        if(values != null && values.length > 0) {
            var idsSelected = values.split(';');
            for(var j = 0; idsSelected.length > j; j++) {
                for(var i = 0; allUsers.length > i; i++) {
                    var temp = allUsers[i].split(',');
                    if(temp[0] ==  idsSelected[j]) {
                        selectedUsers.push(allUsers[i]);
                    }   
                }
            }
            
            var newAllUsers = [];
            for(var i = 0; allUsers.length > i; i++) {
                var add = true;
                var index
                for(var j = 0; selectedUsers.length > j; j++) {
                    if(allUsers[i] == selectedUsers[j]) {
                        add = false;
                    }
                }
                if(add) {
                    newAllUsers.push(allUsers[i]);
                }
            }

            cmp.find(IdRemove+"Id").set("v.value",'');
            
            // set var data
            cmp.set('v.'+IdRemove, newAllUsers);
            cmp.set('v.'+IdAdd,selectedUsers);
            
            cmp.find(IdRemove+"Id").set("v.options", this.dynamicOptions(newAllUsers));
            cmp.find(IdAdd+"Id").set("v.options", this.dynamicOptions(selectedUsers));
        }
    },
})