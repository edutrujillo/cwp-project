({
    doInit : function(component, event, helper) {
    	component.set("v.display", "home");
    },

    changeComponent : function(component, event, helper){
    	component.set("v.display", event.getParam("toOpen"));
	}
})