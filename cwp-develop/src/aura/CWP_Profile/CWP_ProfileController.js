({
	doInit : function(component, event, helper) {
    	component.set("v.display", "profile");
    },

    changeComponent : function(component, event, helper){
    	component.set("v.display", event.getParam("toOpen"));
	}
})