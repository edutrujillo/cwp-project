({
	  getGroups : function(component) {
        
        var action = component.get("c.getMyGroups");    
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.resul_groups", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
      getUser : function(component) {
 		var action = component.get("c.getUserId");
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.resul_user", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
       
	}
})