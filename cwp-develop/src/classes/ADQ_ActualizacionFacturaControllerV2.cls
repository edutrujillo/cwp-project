public  class ADQ_ActualizacionFacturaControllerV2 {
	
/** Para la visualizaci?n en el VF **/
	private Boolean verBotonActualizar;
	private Boolean verContadorPorcentaje;
	private String contadorPorcentaje;
	private Boolean verBotonSAP;
	private Double contadorErrores;
	
	public String idFactura {get;set;} 
	public String FolioFactura {get;set;}
	public Boolean NotieneIdFactura {get;set;}
	
	public Registro_de_actualizacion__c ra{get;set;}  // Ultima actualizaci?n exitosa
	 
	 
	
/**Variables generales**/
	public String periodoSeleccionado{get;set;}
	private ID batchprocessid;
	private ADQ_DivisaUtility divisaUtility;
	
	public ADQ_ActualizacionFacturaControllerV2(){
		verContadorPorcentaje = false;
		ra = new Registro_de_actualizacion__c ();
		Programacion__c ultimaProgModificada = new Programacion__c ();
		NotadeCredito__c ultimaNCModificada = new NotadeCredito__c ();
		Pedido_PE__c ultimoPedidoModificado = new Pedido_PE__c ();
		Factura__c ultimoEncabezadoModificado = new Factura__c ();

		Boolean esNecesarioActualizar;
		periodoSeleccionado = String.valueOf(date.today().month());
		divisaUtility = new ADQ_DivisaUtility();
		
		FolioFactura = '';
		idFactura = Apexpages.currentPage().getParameters().get('pId');
		if (idFactura != '' && idFactura != null){
			System.debug('>>>> idFactura: '+ idFactura);
			for (Factura__c factura01: [SELECT Id,Name FROM Factura__c WHERE Id =:idFactura]){
				FolioFactura  = factura01.Name;
			}
			System.debug('>>> FolioFactura : '+ FolioFactura);
			NotieneIdFactura = false; 
		}else {
			NotieneIdFactura = true;
		}
		
		//Buscamos si se ha modificado alguna programación después de la ?ltima actualización.
		for (Registro_de_actualizacion__c ratemp : [Select Id, Mes_de_actualizacion__c, Tipo_de_cambio_USD__c, 
															Tipo_de_cambio_EUR__c, Errores__c, BatchId__c, Fecha_de_creacion__c, CreatedDate 
														from Registro_de_actualizacion__c 
														where Errores__c = 0 
														Order by CreatedDate desc 
														limit 1]){
			ra = ratemp;
		}

		for (programacion__c ultimaProgModificadatemp : [Select Id, LastModifiedDate FROM Programacion__c Order by LastModifiedDate desc limit 1]){
			ultimaProgModificada = ultimaProgModificadatemp;
		}

		for (NotadeCredito__c ultimaNCModificadatemp : [Select Id, LastModifiedDate FROM NotadeCredito__c Order by LastModifiedDate desc limit 1]){
			ultimaNCModificada = ultimaNCModificadatemp;
		}


		for (Pedido_PE__c ultimoPedidoModificadotemp : [Select Id, LastModifiedDate FROM Pedido_PE__c Order by LastModifiedDate desc limit 1]){
			ultimoPedidoModificado = ultimoPedidoModificadotemp;
		}

		for (Factura__c ultimoEncabezadoModificadotemp : [Select Id, LastModifiedDate FROM Factura__c Order by LastModifiedDate desc limit 1]){
			ultimoEncabezadoModificado = ultimoEncabezadoModificadotemp;
		}

		System.debug('>>>>ra: '+ ra);
		System.debug('>>>>ultimaProgModificada: '+ ultimaProgModificada);
		System.debug('>>>>ultimaNCModificada: '+ ultimaNCModificada);
		System.debug('>>>>ultimoPedidoModificado: '+ ultimoPedidoModificado);
		System.debug('>>>>ultimoEncabezadoModificado: '+ ultimoEncabezadoModificado);
			esNecesarioActualizar = (ra.CreatedDate < ultimaProgModificada.LastModifiedDate) || (ra.CreatedDate < ultimaNCModificada.LastModifiedDate)|| (ra.CreatedDate < ultimoPedidoModificado.LastModifiedDate)|| (ra.CreatedDate < ultimoEncabezadoModificado.LastModifiedDate);
		//Buscamos si hay algún proceso corriendo
		List<AsyncApexJob> la = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
			TotalJobItems, CreatedBy.Email
			from AsyncApexJob where ApexClass.Name = 'ADQ_aBatchActualizacion' AND (Status = 'Processing' OR Status = 'Queued') ];
		if(la.size()>0){
			verContadorPorcentaje = true;
			verBotonActualizar = false;
			verBotonSAP = false;
		}else if(esNecesarioActualizar){
			verBotonActualizar = true;
			verBotonSAP = false;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Se han encontrado programaciones modificadas después de la última actualización, actualice las facturas nuevamente.'));
		}else{
			verBotonActualizar = true;
			verBotonSAP = true;
		}

		
	} 

	public PageReference correBatchActualizacion(){
		//String query = 'Select Id, Name From Account Where Id IN (SELECT Cliente__c FROM Factura__c WHERE Fecha_Inicio__c != null AND CreatedDate >= 2009-01-01T00:00:00.000z AND Cliente__c != \'0017000000TvM40\')  order by Name desc';
		//String query = 'Select Id, Name From Account Where Id IN (\'0017000000WUq4E\') ';
		//String query = 'SELECT Id, Name, Cliente__c, Cliente__r.Name, CurrencyIsoCode, IVA__c, Inicio_de_Facturaci_n__c, Fecha_Inicio__c, Activo__c, Tipo_de_Factura__c, RecordTypeId, CreatedDate FROM Factura__c WHERE Activo__c = true AND Fecha_Inicio__c != null  AND Cliente__c != \'0017000000TvM40\' order by createddate desc';
		//String query = 'SELECT Id, Name, Cliente__c, Cliente__r.Name, CurrencyIsoCode, IVA__c, Inicio_de_Facturaci_n__c, Fecha_Inicio__c, Activo__c, Tipo_de_Factura__c, RecordTypeId, CreatedDate FROM Factura__c limit 1';
		/*
		**
		* Corre actualizacion con un solo Id batch
		**/
	
		String query = 'SELECT Id, Name, Cliente__c, Cliente__r.Name, CurrencyIsoCode, IVA__c, Inicio_de_Facturaci_n__c, Fecha_Inicio__c, Activo__c, Tipo_de_Factura__c, RecordTypeId, CreatedDate,Descripcion__c FROM Factura__c WHERE Activo__c = true AND Fecha_Inicio__c != null  AND Cliente__r.Name != \'CUENTAS ELIMINADAS\' AND Descripcion__c =  \'\' order by createddate desc';
		//String query = 'SELECT Id, Name, Cliente__c, Cliente__r.Name, CurrencyIsoCode FROM Factura__c WHERE  NAME IN (\'00034733-220114\',	\'00034898-060214\',	\'00034899-060214\',	\'00027025-310712\',	\'00027026-310712\',	\'00034893-050214\',	\'00029304-240913\',	\'00029314-250913\',	\'00029313-250913\',	\'00029310-250913\',	\'00029318-250913\',	\'00029319-250913\',	\'00029317-250913\',	\'00036054-220514\',	\'00036055-220514\',	\'00028290-230413\',	\'00028291-230413\',	\'00028289-230413\',	\'00034905-120214\',	\'00034907-120214\',	\'00028293-240413\',	\'00034726-140114\',	\'00036152-280514\') ';
		
		if (idFactura != '' && idFactura != null){
			NotieneIdFactura = false;
			 query = 'SELECT Id, Name, Cliente__c, Cliente__r.Name, CurrencyIsoCode, IVA__c, Inicio_de_Facturaci_n__c, Fecha_Inicio__c, Activo__c, Tipo_de_Factura__c, RecordTypeId, CreatedDate,Descripcion__c FROM Factura__c Where  Descripcion__c =  \'\' AND  Id IN (\''+ idFactura+'\')';
		}else {
			NotieneIdFactura = true;
		}
		System.debug('>>> query: '+ query);
		
		ADQ_aBatchActualizacion actualizacionFacturas = new ADQ_aBatchActualizacion(query, String.valueOf(periodoSeleccionado));
		batchprocessid = Database.executeBatch(actualizacionFacturas,1);
		verContadorPorcentaje = true;	
		verBotonSAP = false;
		verBotonActualizar = false; 
		return null;
	}
	public PageReference pasarPantallaEnvio(){
		Integer periodo = Integer.valueOf(periodoSeleccionado);
		String p = (periodo < 10)?'0' + periodoSeleccionado:periodoSeleccionado;
		return new ApexPages.Pagereference('/apex/ADQ_Migracion?p=' + p);
	}
	
	public void cubre (){
		integer i=0;
		
		
	}
	
	public PageReference revisaEstatusActulizacion(){
		Integer periodo = Integer.valueOf(periodoSeleccionado);
		String p = (periodo < 10)?'0' + periodoSeleccionado:periodoSeleccionado;

		List<AsyncApexJob> la = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
			TotalJobItems, CreatedBy.Email
			from AsyncApexJob where ApexClass.Name = 'ADQ_aBatchActualizacion'  AND (Status = 'Processing' OR Status = 'Queued')];
		System.debug('>>>> la: ' + la);
		if(la.size()>0 ){
			Double cuentaTotales = 0;
			Double cuentaProcesados = 0;
			Double cuentaErrores = 0;
			System.debug('>>>>> la : ' + la);
			for(AsyncApexJob a : la){
				if(a.Status == 'Processing'){
					cuentaTotales += a.TotalJobItems;
					cuentaProcesados += a.JobItemsProcessed;
					cuentaErrores += a.NumberOfErrors;
				}else cuentaTotales = 1;
			}
			verContadorPorcentaje = true;
			Double porcentaje = 0;
			/*
			
			if (la.size()>0){
				porcentaje = (cuentaProcesados*100)/cuentaTotales;
			}else {
				porcentaje = (1*100)/1;
			}
			*/
			porcentaje = (cuentaProcesados*100)/cuentaTotales;
			porcentaje =  divisaUtility.round(porcentaje, 2);
			contadorPorcentaje = porcentaje + ' %';
			return null;
		}
		/*
		else{
			Registro_de_actualizacion__c ra2 = new Registro_de_actualizacion__c ();
			for (Registro_de_actualizacion__c ra2temp : [Select Id, Mes_de_actualizacion__c, Tipo_de_cambio_USD__c, Tipo_de_cambio_EUR__c, Errores__c, BatchId__c, CreatedDate 
																from Registro_de_actualizacion__c where Errores__c = 0 Order by CreatedDate desc limit 1]){
				ra2 = ra2temp;
			}
			//Registro_de_actualizacion__c ra2 = [Select Id, Mes_de_actualizacion__c, Tipo_de_cambio_USD__c, Tipo_de_cambio_EUR__c, Errores__c, BatchId__c, CreatedDate from Registro_de_actualizacion__c where Errores__c = 0 Order by CreatedDate desc limit 1] ;
			if (ra2 != null){
				if(ra2.Id == ra.Id){
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No se actualizaron correctamente las facturas, intentelo de nuevo y consulte a su proveedor.'));
					contadorPorcentaje = 'Actualización finalizada - con errores';
					verBotonActualizar = false;
					return null;
				}else{
					contadorPorcentaje = 'Actualización finalizada';
					//verBotonActualizar = true;
					
					
					if (idFactura != '' && idFactura != null){
						 return new ApexPages.Pagereference('/apex/ADQ_Migracion?p=' + p+'&pId='+idFactura);
					}else {
						return new ApexPages.Pagereference('/apex/ADQ_Migracion?p=' + p);
					}
				}
			}else{
				return new ApexPages.Pagereference('/ADQ_apex/Migracion?p=' + p);
			}

		}
		*/

		else{
			contadorPorcentaje = 'Actualización finalizada';
					//verBotonActualizar = true;
			if (idFactura != '' && idFactura != null){
				 return new ApexPages.Pagereference('/apex/ADQ_Migracion?p=' + p+'&pId='+idFactura);
			}else {
				return new ApexPages.Pagereference('/apex/ADQ_Migracion?p=' + p);
			}

		}
	}

	public List<SelectOption> getListaMeses(){
		List<String> meses = new String[13];
		meses[0] = '--Selecciona--';
		meses[1] = 'Enero';
		meses[2] = 'Febrero';
		meses[3] = 'Marzo';
		meses[4] = 'Abril';
		meses[5] = 'Mayo';
		meses[6] = 'Junio';
		meses[7] = 'Julio'; 
		meses[8] = 'Agosto';
		meses[9] = 'Septiembre';
		meses[10] = 'Octubre';
		meses[11] = 'Noviembre';
		meses[12] = 'Diciembre';
		
		String ps = ''+Date.today().month();
		
		List<SelectOption> listaMeses = new List<SelectOption>();
			/* modificamos el for para poder mostrar todos los meses del anio
		*en el proceso de actualizacion se validara si el mes seleccionado es menor al mes atual que actualice
		*hasta el proximo anio simpre y cuando el tiempo se acerque a los 2 ultimos mses del anio
		*/
		/*
		for(Integer i = Integer.valueOf(ps); i <= 12; i++){
			listaMeses.add(new SelectOption(''+i, meses[i]));
		}
		*/
		
		for(Integer i = 0; i <= 12; i++){
			listaMeses.add(new SelectOption(''+i, meses[i]));
		}
		
		return listaMeses;
	}
	
	Integer count = 0;
                        
    public PageReference incrementCounter() {count++;
            return null;
    }
                    
    public Integer getCount() {
        return count;
    }
    
	public Boolean getVerBotonActualizar(){return verBotonActualizar;}
	public Boolean getVerBotonSAP(){return verBotonSAP;}
	public Boolean getVerContadorPorcentaje(){return verContadorPorcentaje;}
	public String getContadorPorcentaje(){return contadorPorcentaje;}
	public Double getContadorErrores(){return contadorErrores;}
	public String getUltimaActualizacion(){
		if (ra != null){
			return (ra.CreatedDate).format('dd-MM-yyyy \'a las\' HH:mm');
		}else{
			return Datetime.now().format('dd-MM-yyyy \'a las\' HH:mm');
		}
	}	

}